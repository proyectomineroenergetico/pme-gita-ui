import { combineReducers } from "redux";
import edgeDevsReducer from "./data/state/edgedevs/edgedevs.redux.slice";
import loginReducer from "./data/state/auth/auth.redux.slice";
import signupReducer from "./data/state/signup/signup.redux.slice";
import fileUploadReducer from "./data/state/fileUpload/fileupload.redux.slice";
import powerEdgedevsSliceReducer from "./data/state/edgedevs/edgedev-power.redux.slice";
import { EdgeDevState } from "./domain/entity/structures/edgedevs.structures";
import { LoginState, RecoveryPasswordState, SignupState } from "./domain/entity/structures/user.structure";
import { EdgedevPowerState } from "./domain/entity/structures/stats.structure";
import { ModelState } from "./data/state/models/model.redux.slice";
import modelReducer from "./data/state/models/model.redux.slice";
import recoveryPasswordReducer from "./data/state/recovery-password/recovery-password.redux.slice";



export type StoreState = {
  edgeDevsState?: EdgeDevState;
  loginState?: LoginState;
  signupState?: SignupState;
  powerEdgedevsState?:EdgedevPowerState
  modelState?:ModelState
  recoveryPasswordState?:RecoveryPasswordState
}

const appReducer = combineReducers({
  edgeDevsState: edgeDevsReducer,
  loginState: loginReducer,
  signupState: signupReducer,
  fileUploadState: fileUploadReducer,
  powerEdgedevsState: powerEdgedevsSliceReducer,
  modelState: modelReducer,
  recoveryPasswordState: recoveryPasswordReducer
});

const rootReducer = (state:any, action:any) => {
  if(action.type==='auth/logoutUser'){
    state = undefined
  }
  return appReducer(state, action)
}

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
