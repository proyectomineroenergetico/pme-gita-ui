import { IUser, ResponseSignup } from './../../entity/structures/user.structure';
import { FORMS_SCHEMAS } from "../../../presentation/constants/forms.contants";
import AuthEntity from "../../entity/models/auth.entity";
import { SignupGateway } from "../gateways/SignupGateway";

const authEntity = new AuthEntity();

export interface SignupInteractor {
  signup(user:IUser):void;
  handlePasswordInputValue(value: string):string;
  handleEmailInputValue(value: string):string;
}

export function createSignupInteractor(signupGateway: SignupGateway): SignupInteractor {
  return {
    signup(user:IUser):void{
      const errorPassword=this.handlePasswordInputValue(user.password);
      const errorEmail=this.handleEmailInputValue(user.email);
      if(errorEmail.length === 0 && errorPassword.length === 0){
        signupGateway.signup(user);
      }
    },
    handlePasswordInputValue(value: string):string {
      const errorMessage = authEntity.getPasswordErrors(
        value,
        FORMS_SCHEMAS.LOGIN.PASSWORD
      );
      signupGateway.updatePasswordState(value, errorMessage);
      return errorMessage;
    },
    handleEmailInputValue(value: string):string {
      const errorMessage = authEntity.getEmailErrors(
        value,
        FORMS_SCHEMAS.LOGIN.EMAIL
      );
      signupGateway.updateEmailState(value, errorMessage);
      return errorMessage;
    },
  };
}
