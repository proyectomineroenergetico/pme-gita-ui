import { FileUploadGateway } from "../gateways/FileUploadGateway";
import LayoutEntity from "../../entity/models/layout.entity";

const layoutEntity = new LayoutEntity();

export interface FileUploadInteractor {
  managementFile(
    fileType: string,
    whatAreTheValidFileType: Array<string>
  ): void;
  resetFileValues(): void;
}

export function createFileUploadInteractor(
  fileUploadGateway: FileUploadGateway
): FileUploadInteractor {
  return {
    managementFile(fileType: string, whatAreTheValidFileType: Array<string>) {
      const isValidTheUploadedFile = layoutEntity.isValidTheUploadedFile(
        fileType,
        whatAreTheValidFileType
      );
      fileUploadGateway.updateFileInputValue(isValidTheUploadedFile);
    },
    resetFileValues() {
      fileUploadGateway.resetFileValues();
    },
  };
}
