import { ModelGateway } from "../gateways/ModelGateway";
import {
  ModelRequest,
  ModelResponseKMeans,
  ModelResponseSVM,
  ChartsForSVM,
  ModelResponseNN,
} from "../../entity/structures/model.structure";
import ModelEntity from "../../entity/models/model.entity";
import store from "../../../store";
import { KeyValueSelect } from "../../entity/structures/common.structure";
import jwt_decode from "jwt-decode";
import { HTTP_STATUS } from "../../../presentation/constants/api.constants";
import { EdgeDevViewModel } from "../../../presentation/view-model/edgedevs.view-model";
import { EdgeDevInteractor } from "./EdgeDevInteractor";

const modelEntity = new ModelEntity();

export interface ModelInteractor {
  asignTheSelectedCharacteristic(value: string): void;
  // extractFeatures(characteristicRadioValue:string, techniqueRadioValue:string, edgeDevEntity: EdgeDevEntity): void;
  onChangeInputValue(
    value: string,
    key: string,
    selectClassProblem: Array<KeyValueSelect>
  ): void;
  getLabels(): void;
  changeBandValue(index: number, currentValue: boolean): void;
  selectAllBands(value: boolean): void;
  runModelSelected(
    bodyRequest: ModelRequest,
    file: File,
    edgeDevsViewModel: EdgeDevViewModel,
    edgeDevInteractor: EdgeDevInteractor
  ): Promise<void>;
  formatModelResponseKMeans(response: ModelResponseKMeans): void;
  formatModelResponseSVM(response: ModelResponseSVM): void;
  formatModelResponseNN(response: ModelResponseNN): void;
  resetFormState(): void;
  updateHistoryModel(history: Array<any>): void;
  getResultsModelByName(modelName: string, techniqueSelected: string): void;
  getHistoryModel(): void;
  transformHistogramData(data: any): Array<Array<number | string>>;
}

export function createModelInteractor(
  modelGateway: ModelGateway
): ModelInteractor {
  return {
    asignTheSelectedCharacteristic(value: string): void {
      modelGateway.asignTheSelectedCharacteristic(value);
    },
    onChangeInputValue(
      value: string,
      key: string,
      selectClassProblem: Array<KeyValueSelect>
    ): void {
      // let classProblem = [...selectClassProblem]
      if (key === "classProblemValue") {
        if (value) {
          selectClassProblem = selectClassProblem.map((elm) => {
            return {
              ...elm,
              checked: false,
            };
          });
          const pos = selectClassProblem.map((elm) => elm.value).indexOf(value);
          selectClassProblem[pos].checked = true;
          modelGateway.changeSelectClassProblemState(selectClassProblem);
        }
      }
      modelGateway.onChangeInputValue({ value, key });
    },

    getLabels(): void {
      modelGateway.getLabels();
    },

    changeBandValue(index: number, currentValue: boolean): void {
      modelGateway.changeBandValue(index, currentValue);
    },

    selectAllBands(value: boolean): void {
      modelGateway.selectAllBands(value);
    },

    updateHistoryModel(history: Array<any>): void {
      modelGateway.setHistoryModel(history);
    },

    async runModelSelected(
      bodyRequest: ModelRequest,
      file: File,
      edgeDevsViewModel: EdgeDevViewModel,
      edgeDevInteractor: EdgeDevInteractor
    ): Promise<void> {
      // let userId:any=jwt_decode(localStorage.getItem("userToken"));
      if (file) {
        const responseUploadLabels = await modelGateway.updateLabels(file);
        if (responseUploadLabels.payload?.status !== HTTP_STATUS.OK) {
          setTimeout(() => {
            modelGateway.resetResponseUpdateLabel();
          }, 6000);
        }
      }
      // if (responseUploadLabels.payload?.status === HTTP_STATUS.OK) {
      await modelGateway.runModelSelected(bodyRequest);
      edgeDevsViewModel.toggleModal(
        edgeDevInteractor,
        "characteristicsModalOpen",
        !edgeDevsViewModel.characteristicsModalOpen
      );
      // } else {
      //   setTimeout(() => {
      //     modelGateway.resetResponseUpdateLabel();
      //   }, 6000);
      // }
      // const response:any =
    },

    formatModelResponseKMeans(response: ModelResponseKMeans): void {
      let chartConfig: Array<any> =
        modelEntity.setResponseFormatForKMeans(response);
      modelGateway.setKMeansCharts(chartConfig);
    },

    formatModelResponseSVM(response: ModelResponseSVM): void {
      let chartConfig: ChartsForSVM =
        modelEntity.setResponseFormatForSVM(response);
      modelGateway.setSVMCharts(chartConfig);
    },

    formatModelResponseNN(response: ModelResponseNN): void {
      let chartConfig: any = modelEntity.setResponseFormatForNN(response);
      // let histogramConfig:any=modelEntity.getHistogramDataTimeTecnique(response);

      modelGateway.setNNCharts(chartConfig);
      // modelGateway.setHistogramChartForTimeTechnique(histogramConfig)
    },

    resetFormState(): void {
      modelGateway.resetFormState();
    },

    async getResultsModelByName(
      modelName: string,
      techniqueSelected: string
    ): Promise<void> {
      const response: any = await modelGateway.getResultsModelByName(modelName);
      if (modelName.includes("K-MEANS")) {
        this.formatModelResponseKMeans(response.payload);
      } else if (modelName.includes("LINEAR-SVM")) {
        this.formatModelResponseSVM(response.payload);
      } else if (
        modelName.includes("NEURAL-NETWORK") ||
        modelName.includes("RANDOM-FOREST")
      ) {
        this.formatModelResponseNN(response.payload);
      }
    },

    async getHistoryModel(): Promise<void> {
      const historyResponse: any = await modelGateway.getHistoryModel();
      let historyResults: Array<Array<string>> =
        historyResponse?.payload?.results;
      if (historyResults?.length > 0) {
        if (historyResults.length > 10) {
          historyResults.splice(0, historyResults.length - 10);
        }
        let historyModel = historyResults.map((elm: Array<string>) => {
          const historyData = JSON.parse(elm[1]);
          return {
            model_name: elm[0],
            params: "",
            history: historyData.history.map(
              (historyElm: string, index: number) => {
                return {
                  type: "info",
                  message: historyElm,
                  initDate: historyData.date[index],
                };
              }
            ),
            progress: Math.floor(
              historyData.status[historyData.status.length - 1]
            ),
          };
        });
        modelGateway.setHistoryModel(historyModel);
      }
      // else if(modelName.includes('NEURAL-NETWORK')||modelName.includes('RANDOM-FOREST') ){
      //   this.formatModelResponseNN(response.payload)
      // }
    },

    transformHistogramData(data: any): Array<Array<number | string>> {
      return modelEntity.getHistogramDataTimeTecnique(data);
    },
  };
}
