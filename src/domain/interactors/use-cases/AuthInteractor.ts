import { FORMS_SCHEMAS } from "../../../presentation/constants/forms.contants";
import AuthEntity from "../../entity/models/auth.entity";
import { IUser } from "../../entity/structures/user.structure";
import { AuthGateway } from "../gateways/AuthGateway";

const authEntity = new AuthEntity();

export interface AuthInteractor {
  loginUser(user: IUser): void;
  handlePasswordInputValue(value: string): void;
  handleEmailInputValue(value: string): void;
  resetResponseError(): void;
  resetLoginSuccessResponse(): void;
}

export function createAuthInteractor(authGateway: AuthGateway): AuthInteractor {
  return {
    loginUser(user: IUser) {
      authGateway.loginUser(user);
    },
    handlePasswordInputValue(value: string) {
      const errorMessage = authEntity.getPasswordErrors(
        value,
        FORMS_SCHEMAS.LOGIN.PASSWORD
      );
      authGateway.updatePasswordState(value, errorMessage);
    },
    handleEmailInputValue(value: string) {
      const errorMessage = authEntity.getEmailErrors(
        value,
        FORMS_SCHEMAS.LOGIN.EMAIL
      );
      authGateway.updateEmailState(value, errorMessage);
    },
    resetResponseError() {
      authGateway.resetResponseError();
    },
    resetLoginSuccessResponse() {
      authGateway.resetLoginSuccessResponse();
    },
  };
}
