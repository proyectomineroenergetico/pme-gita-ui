import { RecoveryPaswordGateway } from '../gateways/RecoveryPaswordGateway';
import AuthEntity from "../../entity/models/auth.entity";
import { FORMS_SCHEMAS } from '../../../presentation/constants/forms.contants';

const authEntity = new AuthEntity();

export interface RecoveryPasswordInteractor {
  handleEmailRecovery(emailValue:string):void;
  handleResetPasswordValue(password:string):string;
  handleResetConfirmPasswordValue(password:string):string;
  handleResetPasswordAction(password:string, confirmPassword:string, token:string):void;
  getDecodeResetToken(token:string):any;
}

export function createRecoveryPasswordInteractor(recoveryGateway: RecoveryPaswordGateway): RecoveryPasswordInteractor {
  return {
    handleEmailRecovery(emailValue:string):void{
      const errorMessage = authEntity.getEmailErrors(
        emailValue,
        FORMS_SCHEMAS.LOGIN.EMAIL
      );
      recoveryGateway.setErrorEmailRecovery(errorMessage);
      
    },
    handleResetPasswordValue(password:string):string{
      const errorMessage = authEntity.getPasswordErrors(
        password,
        FORMS_SCHEMAS.LOGIN.PASSWORD
      );
      recoveryGateway.setErrorPasswordRecovery(errorMessage);
      return errorMessage;
    },
    handleResetConfirmPasswordValue(password:string):string{
      const errorMessage = authEntity.getPasswordErrors(
        password,
        FORMS_SCHEMAS.LOGIN.PASSWORD
      );
      recoveryGateway.setErrorConfirmPasswordRecovery(errorMessage);
      return errorMessage;
    },

    handleResetPasswordAction(password:string, confirmPassword:string, token:string):void{
      const errorMessageConfirmPwd=this.handleResetConfirmPasswordValue(confirmPassword);
      const errorMessagePwd=this.handleResetPasswordValue(password);
      if(errorMessageConfirmPwd==='' && errorMessagePwd===''){
        if(password!==confirmPassword){
          recoveryGateway.setresponseError('¡Las contraseñas no coinciden!');
        }else{
          recoveryGateway.resetPassword({
            password:password,
            token
          });
        }
      } 
       
    },

    getDecodeResetToken(token:string):any{
      return authEntity.decodedToken(token);
    }
  };
}
