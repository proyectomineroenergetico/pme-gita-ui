import { EdgeDevGateway } from "../gateways/EdgeDevGateway";
export interface EdgeDevInteractor {
  uploadEdgeDevsByFile(file: File, updateCurrentElements:boolean, actionInUpdateMeters?:string): Promise<void>;
  getEdgeDevicesList(): void;
  getInfoByMeter(edgedev: string): void;
  toogleModal(key: string, value: boolean): void;
  changeModalUpdateMeterState(value:boolean):void;
  onChangeInputValue(value:string, key:string):void;
  // extractFeatures(characteristicRadioValue:string, techniqueRadioValue:string, edgeDevEntity: EdgeDevEntity): void;
}

export function createEdgedevInteractor(
  edgeDevGateway: EdgeDevGateway
): EdgeDevInteractor {
  return {
    async uploadEdgeDevsByFile(file: File, updateCurrentElements:boolean, actionInUpdateMeters?:string):Promise<void> {
      await edgeDevGateway.uploadEdgeDevsByFile(file,updateCurrentElements, actionInUpdateMeters);
      edgeDevGateway.resetAfterUpload();
    },
    getEdgeDevicesList() {
      edgeDevGateway.getEdgeDevicesList();
    },

    getInfoByMeter(edgedev: string) {
      edgeDevGateway.getEdgeDeviceInfo(edgedev);
    },

    toogleModal(key: string, value: boolean) {
      edgeDevGateway.toogleModal(key, value);
    },

    changeModalUpdateMeterState(value:boolean):void{
      edgeDevGateway.changeModalUpdateMeterState(value)
    },

    onChangeInputValue(value:string, key:string):void{
      edgeDevGateway.onChangeInputValue({value, key});
    },

    // extractFeatures(characteristicRadioValue:string, techniqueRadioValue:string, edgeDevEntity: EdgeDevEntity): void{
    //   let  characteristicRadioSelected = edgeDevEntity.validateIfRadioButtonIsNotEmpty(
    //     characteristicRadioValue
    //   );
    //   let  techniqueRadioSelected = edgeDevEntity.validateIfRadioButtonIsNotEmpty(
    //     techniqueRadioValue
    //   );
    //   edgeDevGateway.extractFeatures(characteristicRadioSelected, techniqueRadioSelected);
    // },
  };
}
