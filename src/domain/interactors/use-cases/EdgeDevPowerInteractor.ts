import { EdgeDevPowerGateway } from "../gateways/EdgeDevPowerGateway";
import { EdgeDevTargetData } from "../../entity/structures/edgedevs.structures";
import { format } from "date-fns";

export interface EdgeDevPowerInteractor {
  transformDataPower(data: EdgeDevTargetData): Array<Array<number | string>>;
}

export function createEdgedevPowerInteractor(
  edgeDevPowerGateway: EdgeDevPowerGateway
): EdgeDevPowerInteractor {
  return {
    transformDataPower(data: EdgeDevTargetData): Array<Array<number | string>> {
      const dataChart = [];
      const elementsLength = Object.keys(data.Date).length;
      for (let index = 0; index < elementsLength; index++) {
        const dataElement = [];
        dataElement.push(format(new Date(data.Date[index]), "MM/dd/yyyy"));
        dataElement.push(data.Active[index]);
        dataElement.push(data.Reactive[index]);
        dataChart.push(dataElement);
      }
      return dataChart;
    },
  };
}
