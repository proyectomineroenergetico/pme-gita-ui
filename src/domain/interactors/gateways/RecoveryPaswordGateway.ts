import { IUser, ResetPasswordRequest } from "../../entity/structures/user.structure";

export interface RecoveryPaswordGateway {
  setErrorEmailRecovery(message:string):void;
  setErrorPasswordRecovery(message:string):void;
  setErrorConfirmPasswordRecovery(message:string):void;
  setresponseError(error:string):void;
  resetPassword(request:ResetPasswordRequest):void;
}
