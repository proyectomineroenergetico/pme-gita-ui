import { IUser } from "../../entity/structures/user.structure";

export interface AuthGateway {
  updatePasswordState(value: string, errorMessage: string): void;
  updateEmailState(value: string, errorMessage: string): void;
  loginUser(user: IUser): void;
  resetResponseError(): void;
  resetLoginSuccessResponse(): void;
}
