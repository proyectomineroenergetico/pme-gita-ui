export interface FileUploadGateway {
  updateFileInputValue(isValidTheUploadedFile: boolean): void;
  resetFileValues(): void;
}
