import { SchemaValidationRadioGroup } from "../../entity/structures/layout.structure";
import { KeyValue } from "../../entity/structures/common.structure";

export interface EdgeDevGateway {
  uploadEdgeDevsByFile(file: File, updateCurrentElements:boolean, actionInUpdateMeters:string): void;
  getEdgeDevicesList(): void;
  getEdgeDeviceInfo(siccode: string): void;
  toogleModal(key: string, value: boolean): void;
  changeModalUpdateMeterState(value:boolean):void;
  resetAfterUpload():void;
  onChangeInputValue(keyValue:KeyValue):void;
  // extractFeatures(characteristicRadioSelected: SchemaValidationRadioGroup, techniqueRadioSelected: SchemaValidationRadioGroup): void;
}
