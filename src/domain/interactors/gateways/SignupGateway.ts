import { IUser } from "../../entity/structures/user.structure";

export interface SignupGateway {
  updatePasswordState(value: string, errorMessage: string): void;
  updateEmailState(value: string, errorMessage: string): void;
  signup(user:IUser):void;
}
