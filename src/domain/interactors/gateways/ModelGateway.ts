import { KeyValue, KeyValueSelect } from "../../entity/structures/common.structure";
import { ModelRequest, ChartsForSVM } from "../../entity/structures/model.structure";

export interface ModelGateway {
  asignTheSelectedCharacteristic(value: string): void;
  // extractFeatures(characteristicRadioSelected: SchemaValidationRadioGroup, techniqueRadioSelected: SchemaValidationRadioGroup): void;
  onChangeInputValue(keyValue:KeyValue):void;
  getLabels():void;
  changeBandValue(index:number, currentValue: boolean):void;
  selectAllBands(value:boolean):void;
  runModelSelected(bodyRequest:ModelRequest):Promise<any>;
  resetResponseUpdateLabel():void;
  updateLabels(file:File):Promise<any>;
  setKMeansCharts(chartConfig:Array<any>):void;
  setSVMCharts(chartConfig:ChartsForSVM):void;
  setNNCharts(chartConfig:any):void;
  resetFormState():void;
  getHistoryModel():void;
  changeSelectClassProblemState(selectClassProblem: Array<KeyValueSelect>):void;
  setHistoryModel(history:Array<any>):void;
  getResultsModelByName(modelName:string):void;
  setHistogramChartForTimeTechnique(chartData:Array<Array<any>> | undefined):void;
}
