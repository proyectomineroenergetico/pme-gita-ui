import { ResetPasswordRequest, IUser } from "../entity/structures/user.structure";

export default interface IUserRepository {
  sendEmailRecovery(email:string): Promise<Response>;
  updatePassword(request:ResetPasswordRequest):Promise<Response>;
  signup(user:IUser):Promise<Response>;
}
