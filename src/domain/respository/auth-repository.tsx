import { IUser } from "../entity/structures/user.structure";

export default interface AuthRepository {
  login(user: IUser): Promise<Response>;
  showErrorResponseNotification(error: any): void;
}
