export default interface IEdgeDevsRepository {
  uploadEdgeDevsByFile(file: File): Promise<Response>;
  getEdgeDevices(): Promise<Response>;
  getEdgeDeviceInfo(siccode: string): Promise<Response>;
}
