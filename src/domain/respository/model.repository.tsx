import { ModelRequest } from "../entity/structures/model.structure";

export default interface IModelRepository {
  getLabels(): Promise<Response>;
  runModelSelected(bodyRequest:ModelRequest):Promise<Response>;
  getResultsModelByName(mdoelName:string):Promise<Response>;
  getHistoryModel():Promise<Response>;
  uploadLabelsByFile(file: any): Promise<Response> 
}
