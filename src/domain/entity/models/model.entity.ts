import { INPUT_PATTERNS } from "./../../../presentation/constants/forms.contants";

import LayoutEntity from "./layout.entity";
import {
  ResultsKmeans,
  ModelResponseSVM,
  ModelResponseKMeans,
  ChartsForSVM,
  ModelResponseNN,
  ResultsNN,
} from "../structures/model.structure";
import { format } from "date-fns";

export default class ModelEntity extends LayoutEntity {
  public getHistogramDataTimeTecnique(sourceResponse: ResultsNN) {
    const timexAxis = sourceResponse["year-month"].filter(
      (elm, index, array) => array.indexOf(elm) === index
    );

    const labelStrings = sourceResponse.class_by_label.split(",");
    const posibleLabelValues = sourceResponse["label"].filter(
      (elm, index, array) => array.indexOf(elm) === index
    );
    let labelOcurrenciesPositions = sourceResponse["label"].map((label) => {
      let indexes = [],
        i = -1;
      while ((i = sourceResponse["label"].indexOf(label, i + 1)) != -1) {
        indexes.push(i);
      }
      return {
        label,
        indexes,
      };
    });

    let yearMonthOcurrenciesPositions = sourceResponse["year-month"].map(
      (yearMonth) => {
        let indexes = [],
          i = -1;
        while (
          (i = sourceResponse["year-month"].indexOf(yearMonth, i + 1)) != -1
        ) {
          indexes.push(i);
        }
        return {
          yearMonth,
          indexes,
        };
      }
    );
    const labelOcurrenciesPositionsFiltered = posibleLabelValues.map(
      (possibleLabel, index) => {
        return labelOcurrenciesPositions.find(
          (elm, index, array) => elm.label === possibleLabel
        );
      }
    );

    const yearMonthOcurrenciesPositionsFiltered = timexAxis.map(
      (possibleYearMonth) => {
        return yearMonthOcurrenciesPositions.find(
          (elm, index, array) => elm.yearMonth === possibleYearMonth
        );
      }
    );

    let dataJsonData: any = [];
    yearMonthOcurrenciesPositionsFiltered.forEach((elm) => {
      for (
        let index = 0;
        index < labelOcurrenciesPositionsFiltered.length;
        index++
      ) {
        let labelDataByDate: Array<any> = [];
        const filteredArray = labelOcurrenciesPositionsFiltered[
          index
        ].indexes.filter((value) => elm.indexes.includes(value));
        labelDataByDate.push(
          `Etiqueta ${labelOcurrenciesPositionsFiltered[index].label}`
        );
        labelDataByDate.push(
          format(
            new Date(
              +elm.yearMonth.split("-")[0],
              +elm.yearMonth.split("-")[1] - 1,
              1
            ),
            "MM/dd/yyyy"
          )
        );
        labelDataByDate.push(filteredArray.length);
        dataJsonData.push(labelDataByDate);
      }
    });
    return dataJsonData;
  }

  public setResponseFormatForNN(sourceResponse: ModelResponseNN) {
    const timexAxis = sourceResponse.results["year-month"].filter(
      (elm, index, array) => array.indexOf(elm) === index
    ); //.join('|')
    let dataSet: any = [];
    let currentMeter = "";
    let dataDetails: any;
    sourceResponse.results.sicCodes.map((sicCode, index) => {
      if (sicCode !== currentMeter) {
        if (dataDetails) {
          dataSet.push(dataDetails);
        }
        let frtValues = sourceResponse.results.sicCodes
          .map((elm, indexMeter) => {
            if (elm === sicCode) {
              return {
                index: indexMeter,
                meter: elm,
                date: sourceResponse.results["year-month"][indexMeter],
              };
            } else {
              return;
            }
          })
          .filter((elm) => elm !== undefined);
        dataDetails = {
          seriesname: sicCode,
          data: timexAxis
            .map((date) => {
              // return sourceResponse.results.label[index].toString()
              const ocurrencyMeter = frtValues
                .map((elm) => elm.date)
                .indexOf(date);
              if (ocurrencyMeter >= 0) {
                return {
                  value:
                    sourceResponse.results.label[
                      frtValues[frtValues.map((elm) => elm.date).indexOf(date)]
                        .index
                    ].toString(),
                };
              } else {
                return "";
              }
            })
            .filter((elm) => elm !== null),
        };
        currentMeter = sicCode;
      }
    });
    let lineChartData: any = {
      chartData: {
        type: "scrollline2d",
        width: "100%",
        height: "100%",
        dataFormat: "json",
        dataSource: {
          chart: {
            caption: "Resultados para el modelo",
            subcaption: "Data from 2017 to 2019",
            yaxisname: "Predicción",
            xaxisname: "Date",
            plottooltext:
              "<div style='display:inline-block'><b>value: $dataValue</b>, predicted for $seriesName</div>",
            theme: "fusion",
          },
          categories: [
            {
              category: timexAxis.map((elm) => {
                return {
                  label: elm,
                };
              }),
            },
          ],
          dataset: dataSet,
        },
      },
      performanceMetrics: {
        ACC_test: sourceResponse.results?.ACC_test,
        D: sourceResponse.results?.D || undefined,
        Fscore: sourceResponse.results?.Fscore,
        N: sourceResponse.results?.N || undefined,
        UAR_SP: sourceResponse.results?.UAR_SP,
        UAR_dev: sourceResponse.results?.UAR_dev,
      },
    };

    return lineChartData;
  }

  public setResponseFormatForSVM(
    sourceResponse: ModelResponseSVM
  ): ChartsForSVM {
    let bandsKeysResults = Object.keys(sourceResponse.results);
    bandsKeysResults = bandsKeysResults.filter(
      (elm) => elm !== "class_by_label"
    );
    let arrayChartsData: any = [];
    let heatMapChartData: any = {
      type: "heatmap",
      width: "100%",
      height: "100%",
      dataFormat: "json",
      dataSource: {
        chart: {
          caption: "Predicciones por banda",
          // subcaption: "2016-2019",
          theme: "fusion",
          valuefontsize: "12",
          showlabels: "1",
          showvalues: "1",
          showplotborder: "1",
          placexaxislabelsontop: "1",
          mapbycategory: "0",
          showlegend: "0",
          plottooltext:
            "<b>$rowlabel</b> in <b>$columnlabel</b> ha sido clasificado como <b>$displayValue</b>",
          valuefontcolor: "#262A44",
        },
        rows: {
          row: sourceResponse.results[
            bandsKeysResults[0]
          ].predictions.sicCodes.map((elm) => {
            return {
              id: elm,
              label: elm,
            };
          }),
        },
        columns: {
          column: bandsKeysResults.map((col) => {
            return {
              id: col,
              label: col,
            };
          }),
        },
        dataset: [
          {
            data: [],
          },
        ],
        colorrange: {},
      },
    };
    let labelsInPredictions: number[] = [];

    bandsKeysResults.forEach((band: string) => {
      const meterPredictions: string[] =
        sourceResponse.results[band]?.predictions?.sicCodes;
      const labelPredictions: number[] = JSON.parse(
        sourceResponse.results[band]?.predictions?.label
      );
      const right_label: number[] =
        sourceResponse.results[band]?.predictions?.right_label;
      labelsInPredictions = labelsInPredictions.concat(labelPredictions);
      if (Object.keys(sourceResponse.results[band]).length > 1) {
        const accuracies: number[] = JSON.parse(
          sourceResponse.results[band].accuracies
        );
        const precision: number[] = JSON.parse(
          sourceResponse.results[band].precision
        );
        const recall: number[] = JSON.parse(
          sourceResponse.results[band].recall
        );
        let chartData: any = {
          type: "msline",
          width: "100%",
          height: 400,
          dataFormat: "json",
          dataSource: {
            chart: {
              caption: `SVM, resultados para la ${band}`,
              drawcrossline: "1",
              showhovereffect: "1",
              // subcaption: "xxxxxxxxxxxxxxxxxx",
              yaxisname: "componente 1",
              // numbersuffix: " unidades",
              plottooltext: "<b>$dataValue</b> $seriesName",
              theme: "fusion",
            },
            categories: [
              {
                category: accuracies.map((elm, index) => {
                  return { label: `${index + 1}` };
                }),
              },
            ],
            dataset: [
              {
                seriesname: "accuracies",
                data: accuracies.map((elm) => {
                  return { value: elm.toString() };
                }),
              },
              {
                seriesname: "precision",
                data: precision.map((elm) => {
                  return { value: elm.toString() };
                }),
              },
              {
                seriesname: "recall",
                data: recall.map((elm) => {
                  return { value: elm.toString() };
                }),
              },
            ],
            trendlines: [
              {
                line: [
                  {
                    startvalue:
                      sourceResponse.results[band].acc_mean.toString(),
                    color: this.generateRandomColor(),
                    valueOnRight: "1",
                    thickness: "2",
                  },
                  {
                    startvalue:
                      sourceResponse.results[band].best_accuracy.toString(),
                    color: this.generateRandomColor(),
                    valueOnRight: "1",
                    thickness: "2",
                  },
                ],
              },
            ],
          },
        };
        arrayChartsData.push(chartData);
      }
      meterPredictions.map((elm, index) => {
        let objToArray: any = {
          rowid: elm,
          columnid: band,
          value:
            Object.keys(sourceResponse.results[band]).length === 1
              ? labelPredictions[index]
              : labelPredictions[index] === right_label[index]
              ? 1
              : 0,
          displayvalue: `label ${labelPredictions[index]}`,
        };
        if (Object.keys(sourceResponse.results[band]).length > 1) {
          objToArray["colorRangeLabel"] =
            labelPredictions[index] === right_label[index] ? "Good" : "Bad";
        }
        heatMapChartData.dataSource.dataset[0].data.push(objToArray);
      });
    });
    labelsInPredictions = labelsInPredictions.filter(
      (elm, index, array) => array.indexOf(elm) === index
    );
    if (heatMapChartData.dataSource.dataset[0].data[0].colorRangeLabel) {
      heatMapChartData.dataSource.colorrange = {
        gradient: "0",
        color: [
          {
            code: "#00C851",
            minValue: "1",
            maxValue: "2",
            label: "Good",
          },
          {
            code: "#ff4444",
            minValue: "0",
            maxValue: "0.9",
            label: "Bad",
          },
        ],
      };
    } else {
      heatMapChartData.dataSource.colorrange = {
        gradient: "0",
        minvalue: Math.min(...labelsInPredictions),
        code: "#FCFBFF",
        color: labelsInPredictions.map((elm) => {
          return {
            code: this.generateRandomColor(),
            alpha: 50,
            minvalue: elm,
            maxvalue: elm,
          };
        }),
      };
    }
    return { arrayChartsData, heatMapChartData };
  }

  public setResponseFormatForKMeans(
    sourceResponse: ModelResponseKMeans
  ): Array<any> {
    // const jsonCentroids=JSON.parse(sourceResponse.);
    const bandsKeysResults = Object.keys(sourceResponse.results);
    let arrayChartsData: any = [];
    bandsKeysResults.forEach((band: string) => {
      const feature1Array = JSON.parse(sourceResponse.results[band].Feature1);
      const feature2Array = JSON.parse(sourceResponse.results[band].Feature2);
      const sicCodesArray = JSON.parse(sourceResponse.results[band].SicCodes);
      const centroidsBand: Array<Array<number>> = JSON.parse(
        sourceResponse.results[band].centroids
      );
      const labels: number[] = JSON.parse(sourceResponse.results[band].label);
      const labelValues = labels.filter(
        (elm, index, array) => array.indexOf(elm) === index
      );
      // const centroidBand:Array<Array<number>>=JSON.parse(bandsKeysResults)  jsonCentroids[band];
      let possibleXValues: number[] = [...feature1Array];
      centroidsBand.map((elm) => {
        possibleXValues.push(elm[0]);
      });
      let possibleYValues: number[] = [...feature2Array];
      centroidsBand.map((elm) => {
        possibleYValues.push(elm[1]);
      });
      const xaxisminvalue = Math.min(...possibleXValues).toString();
      const xaxismaxvalue = Math.min(...possibleXValues).toString();
      const yaxisminvalue = Math.min(...possibleYValues).toString();
      const chartData = {
        type: "zoomscatter",
        width: "100%",
        height: 400,
        dataFormat: "json",
        dataSource: {
          chart: {
            caption: `K-MEANS, resultados para la ${band}`,
            // subcaption: "xxxxxxxxxxx",
            xaxisname: "componente 2",
            yaxisname: "componente 1",
            xaxisminvalue: xaxisminvalue,
            xaxismaxvalue: xaxismaxvalue,
            showlegend: "1",
            // ynumberprefix: "$prefix",
            yaxisminvalue: yaxisminvalue,
            // xnumbersuffix: "u",
            theme: "fusion",
            plotToolText: "<b>$displayValue</b>",
          },
          dataset: this.extractDataSetKMeans(
            labelValues,
            labels,
            feature1Array,
            feature2Array,
            sicCodesArray,
            sourceResponse.results,
            centroidsBand
          ),
        },
      };
      arrayChartsData.push(chartData);
    });
    return arrayChartsData;
  }

  public extractDataSetKMeans(
    labelValues: number[],
    labels: number[],
    feature1Array: number[],
    feature2Array: number[],
    sicCodesArray: string[],
    resultsResponse: ResultsKmeans,
    centroidBand: Array<Array<number>>
  ) {
    let dataset = [];
    labelValues.forEach((elm) => {
      const generatedColor = this.generateRandomColor();
      let dataSetObject: any = {
        seriesname: elm.toString(),
        anchorbgcolor: generatedColor,
        anchorbordercolor: generatedColor,
        anchorBgAlpha: 50,
        data: [],
      };
      feature1Array.map((feature, index) => {
        if (labels[index] === elm) {
          dataSetObject.data.push({
            x: feature.toString(),
            y: feature2Array[index].toString(),
            displayValue: sicCodesArray[index],
            id: sicCodesArray[index],
          });
        }
      });
      dataset.push(dataSetObject);
    });
    const generatedColor = this.generateRandomColor();
    let dataSetCentroidObject: any = {
      seriesname: `centroids`,
      anchorbgcolor: generatedColor,
      anchorbordercolor: generatedColor,
      anchorBgAlpha: 50,
      data: [],
    };
    centroidBand.map((centroid: number[]) => {
      dataSetCentroidObject.data.push({
        x: centroid[0].toString(),
        y: centroid[1].toString(),
        displayValue: "centroid",
      });
    });
    dataset.push(dataSetCentroidObject);
    return dataset;
  }

  public generateRandomColor(): string {
    return this.generateRandomValueFromRegExp(INPUT_PATTERNS.HEX_COLOR);
  }
}
