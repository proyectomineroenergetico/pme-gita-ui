import {
  FORMS_SCHEMAS,
  RESPONSE_MESSAGES,
} from "../../../presentation/constants/forms.contants";
import { SchemaValidation } from "../structures/layout.structure";
import LayoutEntity from "./layout.entity";


export default class AuthEntity extends LayoutEntity {
  public getEmailErrors(inputValue: string, schema: SchemaValidation): string {
    const errors: Array<string> = this.validateInputValue(inputValue, schema);
    if (errors.length > 0) {
      const error = errors[0];
      if (error === "requiredError") {
        return RESPONSE_MESSAGES.COMMON.REQ_FIELD;
      }
      if (error === "patternError") {
        return RESPONSE_MESSAGES.COMMON.INVALID_EMAIL;
      }
    }
    return "";
  }

  public getPasswordErrors(
    inputValue: string,
    schema: SchemaValidation
  ): string {
    const errors: Array<string> = this.validateInputValue(inputValue, schema);
    if (errors.length > 0) {
      const error = errors[0];
      if (error === "requiredError") {
        return RESPONSE_MESSAGES.COMMON.REQ_FIELD;
      }
      if (error === "patternError") {
        return RESPONSE_MESSAGES.COMMON.INVALID_PASSWORD;
      }
    }
    return "";
  }

  public isValidLoginForm(emailValue: string, passwordValue: string): boolean {
    const emailErrors = this.getEmailErrors(
      emailValue,
      FORMS_SCHEMAS.LOGIN.EMAIL
    );
    const pwdErrors = this.getPasswordErrors(
      passwordValue,
      FORMS_SCHEMAS.LOGIN.PASSWORD
    );
    return emailErrors.length > 0 || pwdErrors.length > 0;
  }

  public decodedToken(token:string):any{
    return this.decodeToken(token)
  }
}
