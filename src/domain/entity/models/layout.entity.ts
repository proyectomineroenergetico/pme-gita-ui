import {
  SchemaValidation,
  SchemaValidationRadioGroup,
} from "../structures/layout.structure";

import RandExp from "randexp";
import jwt_decode from "jwt-decode";


export default class LayoutEntity {
  public isValidTheUploadedFile(
    type: string,
    whatAreTheValidFileType: Array<string>
  ): boolean {
    let isValid = false;
    whatAreTheValidFileType.forEach((fileType) => {
      if (
        fileType === "excel" &&
        (type ===
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
          type === "application/vnd.ms-excel" ||
          type === "text/xml")
      ) {
        isValid = true;
      }
    });
    return isValid;
  }

  public validateInputValue(
    value: string,
    schema: SchemaValidation
  ): Array<string> {
    const errors: Array<string> = [];
    if (schema?.required && (value === null || value === "")) {
      errors.push("requiredError");
    }
    if (schema?.pattern && !this.pattern(schema.pattern, value)) {
      errors.push("patternError");
    }
    if (schema?.minLength && !this.minLength(schema.minLength, value.length)) {
      errors.push("minLength");
    }
    if (schema?.maxLength && !this.maxLength(schema.maxLength, value.length)) {
      errors.push("maxLength");
    }
    return errors;
  }

  public validateIfRadioButtonIsNotEmpty(
    radioName: string
  ): SchemaValidationRadioGroup {
    const radioButtonsCharacteristic = document.getElementsByName(radioName);
    let isSelectedAnyOption = false;
    let radioBtnValue = null;
    for (let index = 0; index < radioButtonsCharacteristic.length; index++) {
      const element: any = radioButtonsCharacteristic[index];
      if (element["checked"]) {
        isSelectedAnyOption = true;
        radioBtnValue = element["value"];
        break;
      }
    }
    return { isSelectedAnyOption, radioBtnValue };
  }

  protected generateRandomValueFromRegExp(regularExpression:string):string{
    return new RandExp(regularExpression).gen();
  }

  public minLength(min: number, length: number): boolean {
    return length >= min;
  }

  public maxLength(max: number, length: number): boolean {
    return length <= max;
  }

  public pattern(pattern: string, value: string): boolean {
    const regularExpression = new RegExp(pattern);
    return regularExpression.test(value);
  }

  protected decodeToken(token:string): any {
    try{
      let decodedToken=jwt_decode(token);
      return {
        isValid:true,
        decodedToken
      }
    }catch(error){
      console.log(error)
      return {
        isValid:false,
        message:error.message
      }
    }
  }
}
