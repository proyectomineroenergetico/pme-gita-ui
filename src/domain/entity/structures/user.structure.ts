export interface IUser {
  name?: string;
  dni?: number;
  email: string;
  password?: string;
}

export interface LoginState {
  emailValue?: string;
  passwordValue?: string;
  emailError?: string;
  passwordError?: string;
  pendingLogin?: boolean;
  responseError?: string;
  successMessage?: any;
}

export interface SignupState {
  emailValue: string;
  passwordValue: string;
  emailError: string;
  passwordError: string;
  pendingSignup:boolean;
  responseSuccess:ResponseSignup;
  responseError:ResponseSignup;
}

export interface SuccessMessageLogin {
  auth_token: string;
  message: string;
  status: string;
}


export interface RecoveryPasswordState {
  emailError:string,
  passwordError:string,
  confirmPasswordError:string,
  responseError: string,
  isSuccessEmailSend:boolean,
  successMessage:string,
  pendingRecovery:boolean;
  isSuccessUpdatePassword:boolean
}

export interface ResetPasswordRequest{
  token:string,
  password:string
}

export interface ResponseSignup{
  message: string;
  auth_token?: string;
  status: number;
}