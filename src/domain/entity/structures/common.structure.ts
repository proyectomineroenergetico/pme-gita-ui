export interface KeyValueSelect{
  text: string,
  value: string,
  checked?:boolean
}

export interface KeyValue{
  key:string,
  value:string
}