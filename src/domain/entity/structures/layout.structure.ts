export interface IMenuItems {
  name: string;
  redirectTo: string;
  isActive?: boolean;
  menuIcon?: string;
}

export interface SchemaValidation {
  required?: boolean;
  pattern?: string;
  minLength?: number;
  maxLength?: number;
}

export interface FileUploadedState {
  isValidTheUploadedFile?: boolean;
}

export interface SchemaValidationRadioGroup {
  isSelectedAnyOption: boolean;
  radioBtnValue: string;
}
