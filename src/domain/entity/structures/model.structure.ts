export interface BandsInfoObject{
  elements: Array<BandsInfo>;
  unity: string;
}

export interface BandsInfo{
  frequency: string,
  name: string,
  enabled:boolean;
  value: 1 | 0
}

export interface IndexValue{
  index:number;
  value: 1|0;
  enabled:boolean
}

export interface ModelRequest{
  selected_feature: string | undefined,
  selected_technique: string | undefined,
  selected_bands: Array<number> | undefined,
  do: string | undefined,
  class_problem: string | undefined,
  recalc_features:boolean
}

export interface ModelResponseKMeans{
  message:string;
  results:ResultsKmeans
}

export interface ModelResponseSVM{
  message:string;
  results:ResultsSVM
}

export interface ModelResponseNN{
  message:string;
  results:ResultsNN;
  status: string;
}

export interface ResultsNN{
  label: number[],
  class_by_label:string,
  sicCodes:string[],
  'year-month':string[],
  ACC_test: number;
  D: number | undefined;
  Fscore: number;
  N: number | undefined;
  UAR_SP: number;
  UAR_dev: number;
}


export interface ResultsKmeans{
  [band:string]:BandsResponseStructure
}

export interface BandsResponseStructure{
  Feature1:string;
  Feature2:string;
  SicCodes:string;
  label: string;
  centroids:string;
}


export interface ResultsSVM{
  [band:string]:BandsResponseStructureSVM
}


export interface BandsResponseStructureSVM{
  acc_mean:number;
  accuracies:string;
  best_accuracy:number;
  precision: string;
  predictions: PredictionsForSVM;
  recall:string;
}

export interface PredictionsForSVM{
  label:string,
  sicCodes:string[],
  right_label?: number[];
}

export interface ChartsForSVM{
  arrayChartsData:Array<any>;
  heatMapChartData: any
}