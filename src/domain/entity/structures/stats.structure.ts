export interface EdgedevPowerState{
  activePowerSchema:FusionChartSchema[],
  powerDataEdgeDev:Array<Array<string | number>>,
  timeSeriesDs:any
}

export interface FusionChartSchema{
  name:string,
  type:string,
  format?:string
}