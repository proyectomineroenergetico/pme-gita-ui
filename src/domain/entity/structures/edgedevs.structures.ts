import { IMenuItems, SchemaValidationRadioGroup } from "./layout.structure";
export interface EdgeDevState {
  edgedevs: Array<string>;
  menuItemsForEdge: IMenuItems[];
  isLoaderShown: boolean;
  responseError: string;
  responseSuccess: string;
  responseSuccessUpdateMeters: string;
  loaderMessage: string;
  characteristicsModalOpen: boolean;
  siccodeTarget: string;
  edgeDevTargetData: string;
  updateEdgesModalIsOpen:boolean;
  actionInUpdateMeters:string;
}

export interface FileUploadRequest{
  file: File,
  uploadEdgeDevsByFile:boolean,
  actionInUpdateMeters?:string
}

export interface EdgeDevsSuccessResponseSlice {
  responseData: EdgeDevsResponse;
  status: number;
  siccode?: string;
}

export interface EdgeDevInfoSuccessResponseSlice{
  responseData: EdgeDevInfoResponse;
  status: number;
  siccode?: string;
}

interface EdgeDevsResponse {
  data: EdgeDevsDataResponse;
  message: string;
  status: string;
}

interface EdgeDevInfoResponse {
  data: string;
  message: string;
  status: string;
}

interface EdgeDevsDataResponse {
  count: number;
  siccodes: Array<string>;
}

export interface EdgeDevTargetData{
  Date: DataPower;
  Active: DataPower;
  Reactive:DataPower;
  Penalty:DataPower;
}

interface DataPower{
  [index: number]: number;
}

export interface EdgeDevRadioSelected{
  characteristic:SchemaValidationRadioGroup,
  technique:SchemaValidationRadioGroup
}