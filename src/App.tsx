import React from "react";
import "./App.css";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import AppNavigationComponent from "./presentation/view/layout/app-navigation.component";
import { ProvideAuth } from "./data/hooks/use-auth.hook";
import { ProvideLayout } from "./data/hooks/layout.hook";

import PrivateRoute from "./presentation/view/layout/private-route.component";
import { createReduxAuthGateway } from "./data/state/auth/ReduxAuthGateway";
import { createAuthInteractor } from "./domain/interactors/use-cases/AuthInteractor";

import store from "./store";
import { Redirect } from "react-router-dom";
import { LoginRedirectComponent } from "./presentation/view/auth/login/login-redirect.component";
import RecoveryComponent from "./presentation/view/auth/recovery/RecoveryComponent";
import ResetPasswordComponent from "./presentation/view/auth/recovery/ResetPasswordComponent";
import SignupComponent from "./presentation/view/auth/signup/SignupComponent";
import { createReduxSignupGateway } from "./data/state/signup/ReduxSignupGateway";
import { createSignupInteractor } from "./domain/interactors/use-cases/SignupInteractor";

const authGateway = createReduxAuthGateway(store.dispatch);
const authInteractor = createAuthInteractor(authGateway);

const signupGateway = createReduxSignupGateway(store.dispatch);
const signupInteractor = createSignupInteractor(signupGateway);

function App() {
  return (
    <ProvideAuth>
      <Router>
        <Switch>
          <Route path="/login">
            <LoginRedirectComponent authInteractor={authInteractor} />
          </Route>
          <Route path="/signup">
            <SignupComponent signupInteractor={signupInteractor}/>
          </Route>
          <Route path="/recovery">
            <RecoveryComponent/>
          </Route>
          <Route path="/reset_password" exact={false}>
            <ResetPasswordComponent/>
          </Route>
          <PrivateRoute exact path="/">
            <Redirect to="/dashboard/edgedevs" />
          </PrivateRoute>
          <PrivateRoute path="/dashboard">
            <ProvideLayout>
              <AppNavigationComponent />
            </ProvideLayout>
          </PrivateRoute>
        </Switch>
      </Router>
    </ProvideAuth>
  );
}

export default App;
