export const HTTP_STATUS = {
  UKNOWN: 0,
  FORBIDDEN: 403,
  ACCEPTED: 202,
  RESET_CONTENT: 205,
  OK: 200,
  InternalServerError: 500,
  NOT_FOUND: 404,
  CONFLICT: 409,
  NO_CONTENT: 204,
  UNAUTHORIZED: 401,
  BAD_REQUEST: 400,
  CREATED: 201,
  FAIL_DEPENDENCY: 424,
};

export const API_ROUTES = {
  UPLOAD_FILE: "upload_file?action=create",
  LOGIN: "login",
  ALL_METERS: "get_meters",
  EDGEDEV_DATA: "get_data_by_meter?siccode=",
  LABELS: "upload_labels_file",
  FEAT_TECH: "features_and_tech",
  HISTORY_MODEL: "hist_by_technique",
  PERFORMANCE: "performance_by_model",
  USER: {
    RECOVERY: "recovery",
    RESET: "reset",
    SIGNUP: "register_user",
  },
};

export const ENVIRONMENT = {
  IP_BASE: "http://ecf4-190-69-34-138.ngrok.io/",
};
