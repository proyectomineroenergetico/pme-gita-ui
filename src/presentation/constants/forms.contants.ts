export const INPUT_PATTERNS = {
  EMAIL: "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$",
  HEX_COLOR: "^(([0-9a-fA-F]{2}){3}|([0-9a-fA-F]){3})$",
  PASSWORD: '^(?=.{6,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\\W)*.*$',
};

export const RESPONSE_MESSAGES = {
  COMMON: {
    REQ_FIELD: "¡Este campo es requerido!",
    INVALID_EMAIL: "¡Correo electrónico inválido!",
    INVALID_PASSWORD: '¡La contraseña debe tener por lo menos 6 caracteres y contener una mayúscula, una minúscula y un número!',
    MIN_LENGTH_6: "¡Mínimo deben ser 6 caracteres!",
  },
};

export const FORMS_SCHEMAS = {
  LOGIN: {
    EMAIL: {
      required: true,
      pattern: INPUT_PATTERNS.EMAIL,
    },
    PASSWORD: {
      required: true,
      pattern: INPUT_PATTERNS.PASSWORD,
    },
  },
};
