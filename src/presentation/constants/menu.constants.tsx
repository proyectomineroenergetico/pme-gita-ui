import LoginComponent from "../view/auth/login/login.component";
import EdgedevsComponent from "../view/edgedevs/edgedevs.component";

import AppNavigationComponent from "../view/layout/app-navigation.component";
import { IMenuItems } from "../../domain/entity/structures/layout.structure";

export interface IRouteParams {
  path: string;
  component: any;
  routes?: IRouteParams[];
}
export const routes: IRouteParams[] = [
  {
    path: "/login",
    component: LoginComponent,
  },
  {
    path: "/dashboard",
    component: AppNavigationComponent,
    routes: [
      {
        path: "/dashboard/edgedevs",
        component: EdgedevsComponent,
      },
    ],
  },
];

export const menuItems: IMenuItems[] = [
  {
    name: "Medidores",
    redirectTo: "/dashboard/edgedevs",
    menuIcon: "network-wired",
  },
  // {
  //   name: "Series",
  //   redirectTo: "/dashboard/series",
  //   menuIcon: "chart-line",
  // },
  // {
  //   name: "Perfil",
  //   redirectTo: "/dashboard/profile",
  //   menuIcon: "user-alt",
  // },
];
