import {
  MDBBtn,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBCol,
  MDBRow,
  MDBNotification,
  MDBIcon,
  MDBCollapseHeader,
  MDBCollapse,
} from "mdbreact";
import React, { Component } from "react";
import CardEmptyEdgeDevs from "./CardEmptyEdgeDevs";
import TableEdgeDevs from "./TableEdgeDevs";
import "./edgedevs.component.css";
import { connect } from "react-redux";
import { EdgeDevInteractor } from "../../../domain/interactors/use-cases/EdgeDevInteractor";
import {
  createEdgeDevViewModel,
  EdgeDevViewModel,
} from "../../view-model/edgedevs.view-model";
import { StoreState } from "../../../reducer";
import { SubmenuNavigation } from "../common/submenu-navigation";
import EdgeDevEntity from "../../../domain/entity/models/edgedevs.entity";
import LoaderComponent from "../common/loader.component";
import SelectModelModal from "./models/select-model.modal";
import {
  createModelViewModel,
  ModelViewModel,
} from "../../view-model/model.view-model";
import { ENVIRONMENT } from "../../constants/api.constants";
import CollapsibleResultModels from "./models/CollapsibleResultModels";
import jwt_decode from "jwt-decode";
import UpdateEdgesModal from "../modal/update-edges.modal";

export type EdgeDevPageProps = {
  edgeDevInteractor: EdgeDevInteractor;
  edgeDevsViewModel?: EdgeDevViewModel;
  modelViewModel?: ModelViewModel;
  edgeDevEntity: EdgeDevEntity;
};

const mapStateToProps = (state: StoreState): Partial<EdgeDevPageProps> => {
  return {
    edgeDevsViewModel: createEdgeDevViewModel(state.edgeDevsState),
    modelViewModel: createModelViewModel(state.modelState),
  };
};

class EdgedevsComponent extends Component<EdgeDevPageProps, {}> {
  constructor(props: EdgeDevPageProps) {
    super(props);
    props.edgeDevsViewModel.getEdgeDevsList(props.edgeDevInteractor);
  }

  componentDidMount() {
    this.connect();
    this.props.modelViewModel.getHistoryModel();
  }

  connect() {
    const ip = `${ENVIRONMENT.IP_BASE}stream`;
    const source = new EventSource(ip);
    let userId: any = jwt_decode(localStorage.getItem("userToken"));
    userId = userId["usr-id"];

    source.addEventListener("running.method", (message: any) => {
      let historyModel = this.props.modelViewModel.historyOfAnalyzedModels.map(
        (elm) => {
          return {
            model_name: elm.model_name,
            history: elm.history,
            params: elm.params,
            progress: Math.floor(elm.progress),
          };
        }
      );

      if (historyModel.length > 10) {
        historyModel.shift();
      }

      let messageData: any = JSON.parse(message["data"]);

      if (userId === messageData.id) {
        let historyItemPos = historyModel
          .map((elm) => elm.model_name)
          .indexOf(messageData.model_name);
        let historyArray;
        if (historyItemPos >= 0) {
          historyArray = historyModel[historyItemPos].history.map(
            (elm: any) => elm
          );
          historyArray.push({
            type: messageData.type || "info",
            message: messageData.message,
            initDate: messageData.date,
          });
          historyModel[historyItemPos]["progress"] = Math.floor(
            messageData.status
          );
          historyModel[historyItemPos].history = historyArray;
        } else {
          historyArray = [];
          historyArray.push({
            type: messageData.type || "info",
            message: messageData.message,
            initDate: messageData.date,
          });
          historyModel.push({
            history: historyArray,
            model_name: messageData.model_name,
            params: null,
            progress: Math.floor(messageData.status),
          });
        }
        this.props.modelViewModel.updateHistoryModel(historyModel);
      }
    });
  }

  render() {
    const {
      edgeDevsViewModel,
      edgeDevInteractor,
      edgeDevEntity,
      modelViewModel,
    } = this.props;
    const {
      menuItemsForEdge,
      isLoadingFile,
      responseError,
      responseSuccess,
      responseSuccessUpdateMeters,
      loaderMessage,
    } = edgeDevsViewModel;

    const isEmptyEdgeDevs =
      edgeDevsViewModel && edgeDevsViewModel?.edgedevs?.length <= 0;

    return (
      <MDBRow
        className={
          !isEmptyEdgeDevs ? "align-items-center" : "justify-content-center"
        }
      >
        {responseError !== null && (
          <MDBNotification
            show
            fade
            bodyClassName="p-3 px-5 white-text"
            className="danger-color"
            closeClassName="white-text"
            titleClassName="danger-color-dark white-text"
            title="Error"
            message={responseError}
            autohide={5000}
            style={{
              position: "fixed",
              top: "10px",
              right: "10px",
              zIndex: 9999,
            }}
          />
        )}
        {responseSuccess !== null ||
          (responseSuccessUpdateMeters !== null && (
            <MDBNotification
              show
              fade
              bodyClassName="p-3 px-5 white-text"
              className="success-color"
              closeClassName="white-text"
              titleClassName="success-color-dark white-text"
              title="Medidores cargados"
              message={responseSuccess || responseSuccessUpdateMeters}
              autohide={4000}
              style={{
                position: "fixed",
                top: "10px",
                right: "10px",
                zIndex: 9999,
              }}
            />
          ))}
        {isLoadingFile && (
          <div className="position-fixed loaderEdges">
            <LoaderComponent hint={loaderMessage} type="LOADER-IMG" />
          </div>
        )}
        {/* <MDBCol
          size="12"
          className="sticky-secheader position-sticky mt-n3 px-0 mb-2"
        >
          {!isEmptyEdgeDevs && menuItemsForEdge && (
            <SubmenuNavigation menuItems={menuItemsForEdge} />
          )}
        </MDBCol> */}
        <MDBCol
          className="mt-3 pb-4 px-2 px-md-3"
          xl={!isEmptyEdgeDevs ? "12" : "5"}
          size="12"
        >
          <MDBCard className="w-100">
            {isEmptyEdgeDevs && (
              <MDBCardImage
                className="img-fluid"
                src={process.env.PUBLIC_URL + "/img/uploadEdgedevs.jpg"}
                waves
              />
            )}
            <MDBCardBody className="px-1 px-md-3">
              {!isEmptyEdgeDevs && (
                <div className="position-absolute meterButtonUpdate">
                  <div className="buttonHeader z-depth-1 d-flex align-items-center">
                    Dispositivos
                    <div className="ml-auto">
                      <MDBBtn
                        onClick={() =>
                          edgeDevsViewModel.changeModalUpdateMeterState(
                            edgeDevInteractor
                          )
                        }
                        className="d-none d-md-inline-block"
                        outline
                        color="elegant"
                        size="sm"
                      >
                        Actualizar medidores
                      </MDBBtn>
                      <MDBIcon
                        className="elegant-text d-md-none d-inline-block mx-2"
                        icon="redo-alt"
                      />

                      <MDBBtn
                        className="d-none d-md-inline-block"
                        onClick={() => {
                          this.props.modelViewModel.resetState();
                          edgeDevsViewModel.toggleModal(
                            edgeDevInteractor,
                            "characteristicsModalOpen",
                            !edgeDevsViewModel.characteristicsModalOpen
                          );
                        }}
                        color="elegant"
                        size="sm"
                      >
                        Seleccionar modelo
                      </MDBBtn>
                      <MDBIcon
                        className="elegant-text d-md-none d-inline-block mx-2"
                        icon="layer-group"
                      />
                    </div>
                  </div>
                </div>
              )}
              {!isEmptyEdgeDevs ? (
                <TableEdgeDevs
                  edgeDevInteractor={edgeDevInteractor}
                  edgeDevsViewModel={edgeDevsViewModel}
                />
              ) : (
                <CardEmptyEdgeDevs
                  edgeDevViewModel={edgeDevsViewModel}
                  edgeDevInteractor={edgeDevInteractor}
                />
              )}
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
        <hr className="w-100" />
        {modelViewModel.historyOfAnalyzedModels?.length > 0 && (
          <MDBCol className="mt-5 pb-4 px-2 px-md-3" size="12">
            <MDBCard className="w-100">
              <MDBCardBody className="px-1 px-md-3">
                <div className="position-absolute meterButtonUpdate">
                  <div className="buttonHeader z-depth-1 d-flex align-items-center">
                    Análisis de resultados de modelos
                    <MDBBtn
                      onClick={() => {
                        modelViewModel.getHistoryModel();
                        this.connect();
                      }}
                      className="ml-auto"
                      color="elegant"
                      outline
                      size="sm"
                    >
                      <MDBIcon className="elegant-text mx-2" icon="redo-alt" />
                      Actualizar historial
                    </MDBBtn>
                  </div>
                </div>
                {/* <ResultModelAnalysis/> */}
                <CollapsibleResultModels modelViewModel={modelViewModel} />
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
        )}

        {!isEmptyEdgeDevs && (
          <>
            <SelectModelModal
              edgeDevEntity={edgeDevEntity}
              edgeDevsViewModel={edgeDevsViewModel}
              edgeDevInteractor={edgeDevInteractor}
            />
            <UpdateEdgesModal
              edgeDevsViewModel={edgeDevsViewModel}
              edgeDevInteractor={edgeDevInteractor}
              isModalOpen={edgeDevsViewModel.updateEdgesModalIsOpen}
            />
          </>
        )}
      </MDBRow>
    );
  }
}
export default connect<{}, {}, Partial<EdgeDevPageProps>>(mapStateToProps)(
  EdgedevsComponent
);
