import React, { useEffect } from 'react'
import { createModelViewModel } from '../../../view-model/model.view-model';
import { StoreState } from '../../../../reducer';
import { useSelector } from 'react-redux';
import { MDBRow, MDBCol } from 'mdbreact';
import KMeansResults from './KMeansResults';
import SVMResults from './SVMResults';
import NetworkNeuralResults from './NetworkNeuralResults';

type ResultModelAnalysisProps={
  techniqueSelected:string
}

export default function ResultModelAnalysis(props:ResultModelAnalysisProps) {
  const {techniqueSelected}=props;
  const modelViewModel = createModelViewModel(
    useSelector((state: StoreState) => state.modelState)
  );

  const selectModelResultView=()=>{
    switch (techniqueSelected) {
      case 'K-MEANS':
        return <KMeansResults modelViewModel={modelViewModel}/>
      
      case 'LINEAR-SVM':
        return <SVMResults modelViewModel={modelViewModel}/>

      case 'NEURAL-NETWORK':
        return <NetworkNeuralResults modelViewModel={modelViewModel}/>

      case 'RANDOM-FOREST':
        return <NetworkNeuralResults modelViewModel={modelViewModel}/>
    
      default:
        break;
    }
  }

  return (
    <MDBRow className="m-0 w-100">
      <MDBCol size="12">
        {techniqueSelected && selectModelResultView()}
      </MDBCol>
    </MDBRow>
  )
}
