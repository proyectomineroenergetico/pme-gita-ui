import { MDBRow } from 'mdbreact'
import React, { useEffect, Component } from 'react'
import { ModelViewModel } from '../../../view-model/model.view-model'
import FusionCharts from 'fusioncharts';
import Charts from 'fusioncharts/fusioncharts.charts';
import ReactFC from 'react-fusioncharts';
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import * as ZoomScatter from 'fusioncharts/fusioncharts.zoomscatter';


ReactFC.fcRoot(FusionCharts, Charts,ZoomScatter, FusionTheme);

type KMeansResultProps={
  modelViewModel:ModelViewModel
}

export default class KMeansResults extends Component<KMeansResultProps, any> {

  componentDidMount(){
    // this.props.modelViewModel.formatModelResponse()
  }

  render(){
    return (
      <MDBRow>
        {this.props?.modelViewModel?.KMeansCharts?.map((chart, index)=>{
          return (
            <div className="col-6 p-3" key={`chartKmeans${index}`} style={{height:400}}>
              <div className="w-100 h-100">
                <ReactFC {...chart} />
              </div>
            </div>
          )
        })}
      </MDBRow>
    )
  }
}