import React, { Fragment, useEffect, FormEvent, useState } from "react";
import {
  MDBModal,
  MDBModalHeader,
  MDBCloseIcon,
  MDBModalBody,
  MDBInput,
  MDBModalFooter,
  MDBRow,
  MDBCol,
  MDBBtn,
  MDBSelect,
  MDBSelectInput,
  MDBSelectOption,
  MDBSelectOptions,
  MDBTooltip,
  MDBNotification,
} from "mdbreact";
import { InputErrorComponent } from "../../layout/InputError.component";
import { EdgeDevPageProps } from "../edgedevs.component";
import { createModelViewModel } from "../../../view-model/model.view-model";
import { useSelector } from "react-redux";
import { StoreState } from "../../../../reducer";
import { FileUploadComponent } from "../../common/file-upload.component";
import { HTTP_STATUS } from "../../../constants/api.constants";

export default function SelectModelModal(props: EdgeDevPageProps) {
  const { edgeDevsViewModel, edgeDevInteractor } = props;
  const { characteristicsModalOpen } = edgeDevsViewModel;

  const modelViewModel = createModelViewModel(
    useSelector((state: StoreState) => state.modelState)
  );
  useEffect(() => {
    modelViewModel.getLabels();

    return () => {
      setFile(null);
      setisValidTheLabelUploadedFile(null);
    };
  }, []);

  const [isValidTheLabelUploadedFile, setisValidTheLabelUploadedFile] =
    useState(null);
  const [showLabelFileError, setshowLabelFileError] = useState(false);
  const [file, setFile] = useState(null);
  const handlerValidFile = (isValid: boolean) => {
    if (!isValid) {
    }
    setisValidTheLabelUploadedFile(isValid);
  };

  const validateForm = () => {
    if (
      modelViewModel.characteristicRadioValue === undefined ||
      modelViewModel.techniqueRadioValue === undefined ||
      modelViewModel.doRadioValue === undefined
    ) {
      return true;
    } else {
      if (modelViewModel.characteristicRadioValue !== "TIME") {
        const bandsLength = modelViewModel.bands.filter(
          (elm) => elm.enabled === true
        ).length;
        if (bandsLength === 0) {
          return true;
        } else if (
          modelViewModel.techniqueRadioValue === "LINEAR-SVM" &&
          modelViewModel.classProblemValue === undefined
        ) {
          return true;
        }
      } else if (
        modelViewModel.techniqueRadioValue === "K-MEANS" ||
        modelViewModel.techniqueRadioValue === "LINEAR-SVM" ||
        modelViewModel.classProblemValue === undefined
      ) {
        return true;
      }
    }
    return false;
  };

  const handleOnClickApplyModel = () => {
    // (false && true && true )|| true || true
    if (
      Object.keys(modelViewModel.labelsFound).length === 0 &&
      modelViewModel.techniqueRadioValue !== "K-MEANS"

      //   &&
      //   !file) ||
      // !file ||
      // !isValidTheLabelUploadedFile
    ) {
      setshowLabelFileError(true);
      setTimeout(() => {
        setshowLabelFileError(false);
      }, 8000);
    } else {
      modelViewModel.runModelSelected(
        file,
        edgeDevsViewModel,
        edgeDevInteractor
      );
    }
  };

  return (
    <MDBModal
      inline={false}
      overflowScroll={false}
      noClickableBodyWithoutBackdrop={true}
      isOpen={characteristicsModalOpen}
      size="lg"
    >
      {showLabelFileError && (
        <MDBNotification
          show
          fade
          bodyClassName="p-3 px-5 white-text"
          className="danger-color"
          closeClassName="white-text"
          titleClassName="danger-color-dark white-text"
          title="Etiquetas no encontradas"
          message={
            "Actualmente no se han cargado etiquetas para continuar con la evaluación del modelo seleccionado o el archivo cargado en el formulario no tiene un formato válido, carga un archivo en el primer paso de 'Selección de etiquetas' válido, para continuar con la ejecución del modelo."
          }
          autohide={7000}
          style={{
            position: "fixed",
            top: "10px",
            right: "10px",
            zIndex: 9999,
          }}
        />
      )}
      {modelViewModel.responseUpdateLabel &&
        modelViewModel.responseUpdateLabel.status ===
          HTTP_STATUS.BAD_REQUEST && (
          <MDBNotification
            show
            fade
            bodyClassName="p-3 px-5 white-text"
            className="danger-color"
            closeClassName="white-text"
            titleClassName="danger-color-dark white-text"
            title="Etiquetas no cargadas"
            message={modelViewModel.responseUpdateLabel.data}
            autohide={5000}
            style={{
              position: "fixed",
              top: "10px",
              right: "10px",
              zIndex: 9999,
            }}
          />
        )}
      <MDBModalHeader>
        <div className="d-flex">
          <span className="flex-1">Opciones de modelo</span>
          <MDBCloseIcon
            onClick={() =>
              edgeDevsViewModel.toggleModal(
                edgeDevInteractor,
                "characteristicsModalOpen",
                !edgeDevsViewModel.characteristicsModalOpen
              )
            }
          />
        </div>
      </MDBModalHeader>
      <MDBModalBody>
        <p>Selección de etiquetas</p>
        {Object.keys(modelViewModel.labelsFound).length > 0 && (
          <span>
            Actualmente no has cargado un conjunto de etiquetas, adjunta un
            archivo válido para las etiquetas a continuación:{" "}
          </span>
        )}
        <span>
          <strong>¿Deseas actualizar las etiquetas existentes?</strong> Esta
          acción está habilitada por defecto y va a actualizar los datos de las
          etiquetas existentes y agregar las nuevas. Si escoges que no, se
          borrarán las etiquetas actuales y se cargarán las que acabas de
          adjuntar con el archivo.{" "}
        </span>
        <div className="d-flex">
          <div className="col-4">
            <MDBInput
              label="Sí"
              type="radio"
              value="YES-LABELS"
              checked={modelViewModel.labelsRadioValue === "YES-LABELS"}
              name="labelRadioName"
              id="yesLabelOption"
              onChange={(event: any) =>
                modelViewModel.onChangeValue(
                  event.target.value,
                  "labelsRadioValue"
                )
              }
            />
          </div>
          <div className="col-8 d-flex align-items-center mb-3">
            <MDBInput
              label="No"
              type="radio"
              disabled={!(Object.keys(modelViewModel.labelsFound).length > 0)}
              value="NO-LABELS"
              checked={modelViewModel.labelsRadioValue === "NO-LABELS"}
              name="labelRadioName"
              id="noLabelOption"
              onChange={(event: any) =>
                modelViewModel.onChangeValue(
                  event.target.value,
                  "labelsRadioValue"
                )
              }
            />
            {!(Object.keys(modelViewModel.labelsFound).length > 0) && (
              <span
                style={{ fontSize: 13, fontStyle: "italic", marginLeft: 10 }}
              >
                (Desactivado mientras no se hayan cargado etiquetas)
              </span>
            )}
          </div>
        </div>
        {modelViewModel.labelsRadioValue !== "NO-LABELS" && (
          <FileUploadComponent
            isValidTheUploadedFile={(isValid: boolean) =>
              handlerValidFile(isValid)
            }
            onUpdateFile={(file: File) => setFile(file)}
            validFiles={["excel", "csv"]}
          />
        )}
        {/* {(file || modelViewModel.labelsRadioValue === "NO-LABELS") && (
          <Fragment> */}
        <hr className="w-100" />
        <p>Selecciona una de las siguientes características</p>
        <div className="position-relative d-flex">
          <div className="col-6">
            <MDBInput
              label="Características basadas en frecuencia"
              type="radio"
              value="FRECUENCY"
              name="deviceCharacteristicGroup"
              checked={modelViewModel.characteristicRadioValue === "FRECUENCY"}
              id="frequencyRadio"
              onChange={(event: any) =>
                modelViewModel.onChangeValue(
                  event.target.value,
                  "characteristicRadioValue"
                )
              }
            />
          </div>
          <div className="col-6">
            <MDBInput
              className="ml-3"
              label="Características basadas en tiempo"
              type="radio"
              value="TIME"
              name="deviceCharacteristicGroup"
              checked={modelViewModel.characteristicRadioValue === "TIME"}
              id="timeRadio"
              onChange={(event: any) =>
                modelViewModel.onChangeValue(
                  event.target.value,
                  "characteristicRadioValue"
                )
              }
            />
          </div>

          <InputErrorComponent
            className="errorRadioButtonLabel"
            errorMessage={modelViewModel.errorMessageCharacteristic}
          />
        </div>
        {/* </Fragment>
        )} */}

        {modelViewModel.characteristicRadioValue && (
          <div className="p-3">
            <MDBInput
              onChange={(evt: any) =>
                modelViewModel.onChangeValue(
                  evt.target.checked,
                  "recalc_features"
                )
              }
              label="¿Recalcular características?"
              type="checkbox"
              id="checkbox1"
            />
          </div>
        )}

        {modelViewModel.characteristicRadioValue && (
          <Fragment>
            <hr className="w-100" />
            <p>Selecciona una de las siguientes técnicas</p>
            <div className="position-relative d-flex">
              {modelViewModel.characteristicRadioValue === "TIME" && (
                <Fragment>
                  <div className="col-6">
                    <MDBInput
                      label="Técnica Random Forest"
                      type="radio"
                      value="RANDOM-FOREST"
                      checked={
                        modelViewModel.techniqueRadioValue === "RANDOM-FOREST"
                      }
                      name="deviceTechniqueGroup"
                      id="randomForesTechnique"
                      onChange={(event: any) =>
                        modelViewModel.onChangeValue(
                          event.target.value,
                          "techniqueRadioValue"
                        )
                      }
                    />
                  </div>
                  <div className="col-6">
                    <MDBInput
                      label="Técnica Redes Neuronales"
                      type="radio"
                      value="NEURAL-NETWORK"
                      checked={
                        modelViewModel.techniqueRadioValue === "NEURAL-NETWORK"
                      }
                      name="deviceTechniqueGroup"
                      id="neuronalNetworkTechnique"
                      onChange={(event: any) =>
                        modelViewModel.onChangeValue(
                          event.target.value,
                          "techniqueRadioValue"
                        )
                      }
                    />
                  </div>
                </Fragment>
              )}
              {modelViewModel.characteristicRadioValue === "FRECUENCY" && (
                <Fragment>
                  <div className="col-6">
                    <MDBInput
                      label="Técnica K-Means"
                      type="radio"
                      value="K-MEANS"
                      name="deviceTechniqueGroup"
                      checked={modelViewModel.techniqueRadioValue === "K-MEANS"}
                      id="kmeansTechnique"
                      onChange={(event: any) =>
                        modelViewModel.onChangeValue(
                          event.target.value,
                          "techniqueRadioValue"
                        )
                      }
                    />
                  </div>
                  <div className="col-6">
                    <MDBInput
                      label="Técnica svm"
                      type="radio"
                      value="LINEAR-SVM"
                      checked={
                        modelViewModel.techniqueRadioValue === "LINEAR-SVM"
                      }
                      name="deviceTechniqueGroup"
                      id="svmTechnique"
                      onChange={(event: any) =>
                        modelViewModel.onChangeValue(
                          event.target.value,
                          "techniqueRadioValue"
                        )
                      }
                    />
                  </div>
                </Fragment>
              )}
              <InputErrorComponent
                className="errorRadioButtonLabel"
                errorMessage={modelViewModel.errorMessageTechnique}
              />
            </div>
          </Fragment>
        )}

        {(modelViewModel.techniqueRadioValue === "K-MEANS" ||
          modelViewModel.techniqueRadioValue === "LINEAR-SVM") &&
          modelViewModel.characteristicRadioValue === "FRECUENCY" && (
            <Fragment>
              <hr className="w-100" />
              <p>Selecciona las bandas del modelo</p>
              <div className="d-flex align-items-center justify-content-center">
                {modelViewModel.bands.map((elm, index) => {
                  return (
                    <div
                      key={index}
                      className="d-flex flex-wrap flex-column align-items-center justify-content-center mx-1"
                    >
                      <span style={{ fontSize: 12 }}>{elm.name}</span>
                      <MDBTooltip domElement placement="bottom">
                        <div
                          onClick={() =>
                            modelViewModel.changeBandValue(index, elm.enabled)
                          }
                          className={`rounded-circle mx-3 pointer ${
                            elm.enabled ? "green lighten-2" : "red lighten-2"
                          }`}
                          style={{ width: 30, height: 30 }}
                        ></div>
                        <span style={{ fontSize: 10 }}>
                          {elm.frequency} <span> Hz</span>
                        </span>
                      </MDBTooltip>
                    </div>
                  );
                })}
              </div>
              <div className="text-center mt-2">
                <MDBInput
                  label="Seleccionar todas"
                  getValue={(e: any) => modelViewModel.selectAllBands(e)}
                  type="checkbox"
                  id="selectAllBandsCheckbox"
                />
              </div>
            </Fragment>
          )}
        {((modelViewModel.characteristicRadioValue === "TIME" &&
          (modelViewModel.techniqueRadioValue === "RANDOM-FOREST" ||
            modelViewModel.techniqueRadioValue === "NEURAL-NETWORK")) ||
          (modelViewModel.characteristicRadioValue === "FRECUENCY" &&
            (modelViewModel.techniqueRadioValue === "K-MEANS" ||
              modelViewModel.techniqueRadioValue === "LINEAR-SVM") &&
            modelViewModel.bands.filter((elm) => elm.enabled === true).length >
              0)) && (
          <Fragment>
            <hr className="w-100" />
            <p>Selecciona una acción a realizar con el modelo</p>
            <div className="d-flex">
              <div className="col-6">
                <MDBInput
                  label="Entrenar y clasificar"
                  type="radio"
                  value="fit_and_predict"
                  name="do_model"
                  checked={modelViewModel.doRadioValue === "fit_and_predict"}
                  id="fit_predict_radio"
                  onChange={(event: any) =>
                    modelViewModel.onChangeValue(
                      event.target.value,
                      "doRadioValue"
                    )
                  }
                />
              </div>
              <div className="col-6">
                <MDBInput
                  label="Clasificar"
                  type="radio"
                  value="predict"
                  name="do_model"
                  checked={modelViewModel.doRadioValue === "predict"}
                  id="predict_radio"
                  onChange={(event: any) =>
                    modelViewModel.onChangeValue(
                      event.target.value,
                      "doRadioValue"
                    )
                  }
                />
              </div>
            </div>
          </Fragment>
        )}
        {modelViewModel.doRadioValue &&
          ((modelViewModel.characteristicRadioValue === "TIME" &&
            (modelViewModel.techniqueRadioValue === "RANDOM-FOREST" ||
              modelViewModel.techniqueRadioValue === "NEURAL-NETWORK")) ||
            (modelViewModel.characteristicRadioValue !== "TIME" &&
              modelViewModel.techniqueRadioValue === "LINEAR-SVM")) && (
            <Fragment>
              <hr className="w-100" />
              <p>Selecciona un tipo de problema</p>

              <MDBSelect
                getValue={(event: any) =>
                  modelViewModel.onChangeValue(event[0], "classProblemValue")
                }
              >
                <MDBSelectInput selected="Escoge una opción" />
                <MDBSelectOptions>
                  <MDBSelectOption disabled>Escoge una opción</MDBSelectOption>
                  {modelViewModel.selectClassProblem.map((elm, index) => {
                    return (
                      <MDBSelectOption
                        checked={elm.checked}
                        key={`problemClass${index}`}
                        value={elm.value}
                      >
                        {elm.text}
                      </MDBSelectOption>
                    );
                  })}
                </MDBSelectOptions>
              </MDBSelect>
            </Fragment>
          )}
      </MDBModalBody>
      <MDBModalFooter>
        <MDBRow className="w-100">
          <MDBCol size="6">
            <MDBBtn
              className="w-100"
              color="elegant"
              outline
              onClick={() =>
                edgeDevsViewModel.toggleModal(
                  edgeDevInteractor,
                  "characteristicsModalOpen",
                  !edgeDevsViewModel.characteristicsModalOpen
                )
              }
            >
              Cancelar
            </MDBBtn>
          </MDBCol>
          <MDBCol className="w-100" size="6">
            <MDBBtn
              disabled={validateForm()}
              className="w-100"
              color="elegant"
              onClick={() => handleOnClickApplyModel()}
            >
              Aplicar
            </MDBBtn>
          </MDBCol>
        </MDBRow>
      </MDBModalFooter>
    </MDBModal>
  );
}
