import React, { useState, useEffect } from 'react'
import { MDBModal, MDBModalHeader, MDBCloseIcon, MDBModalBody } from 'mdbreact'
import { useDispatch } from 'react-redux';
import { getResultsModelByName } from '../../../../data/state/models/model.redux.slice';
import ResultModelAnalysis from "../models/ResultModelAnalysis";
import { ModelViewModel } from '../../../view-model/model.view-model';

type ModelResultsModalProps={
  onCloseModal(isOpen:boolean):void;
  modelName:string;
  modelViewModel:ModelViewModel
}

export default function ModelResultsModal(props:ModelResultsModalProps) {
  const {modelName, modelViewModel}=props;
  const [resultModelModalOpen, setresultModelModalOpen] = useState(true);
  const [techniqueSelected, settechniqueSelected] = useState('')
  const dispatch = useDispatch()

  useEffect(() => {
    if(modelName.includes('LINEAR-SVM')){
      settechniqueSelected('LINEAR-SVM')
    }else if(modelName.includes('K-MEANS')){
      settechniqueSelected('K-MEANS')
    }else if(modelName.includes('NEURAL-NETWORK')){
      settechniqueSelected('NEURAL-NETWORK')
    }else if(modelName.includes('RANDOM-FOREST')){
      settechniqueSelected('RANDOM-FOREST')
    }
    modelViewModel.getResultsModelByName(modelName, techniqueSelected)
  }, [modelName])

  return (
    <MDBModal
      inline={false}
      overflowScroll={false}
      noClickableBodyWithoutBackdrop={true}
      isOpen={resultModelModalOpen}
      size="fluid"
    >
      <MDBModalHeader>
        <div className="d-flex">
          <span className="flex-1">Resultados del modelo</span>
          <MDBCloseIcon
            onClick={() =>
              props.onCloseModal(!resultModelModalOpen)
            }
          />
        </div>
      </MDBModalHeader>
      <MDBModalBody>
        {techniqueSelected && <ResultModelAnalysis techniqueSelected={techniqueSelected}/>}
        
      </MDBModalBody>
    </MDBModal>
  )
}
