import React, { useEffect, useState } from "react";
import { ModelViewModel } from "../../../view-model/model.view-model";
import {
  MDBRow,
  MDBSelect,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
  MDBSelectInput,
  MDBSelectOptions,
} from "mdbreact";
import ReactFC from "react-fusioncharts";
import FusionCharts from "fusioncharts";
import Charts from "fusioncharts/fusioncharts.charts";
import * as ZoomLine from "fusioncharts/fusioncharts.zoomline";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
import TimeSeries from "fusioncharts/fusioncharts.timeseries";
// ReactFC.fcRoot(FusionCharts, TimeSeries);

ReactFC.fcRoot(FusionCharts, Charts, ZoomLine, FusionTheme, TimeSeries);

type NetworkNeuralResultProps = {
  modelViewModel: ModelViewModel;
};
export default function NetworkNeuralResults(props: NetworkNeuralResultProps) {
  const { modelViewModel } = props;
  const [targetChartData, settargetChartData] = useState({});
  const [meterSelect, setmeterSelect] = useState([]);

  const [timeseriesDs, settimeseriesDs] = useState({
    type: "timeseries",
    renderAt: "container",
    width: "100%",
    height: "400",
    dataSource: {
      caption: { text: `Ocurrencia de medidores en etiquetas por fecha` },
      data: null,
      yAxis: [
        {
          plot: {
            value: "Medidores",
            type: "column",
          },
        },
      ],
      series: "Etiqueta",
    },
  });

  useEffect(() => {
    if (modelViewModel.NNCharts?.chartData && meterSelect?.length === 0) {
      setmeterSelect(
        modelViewModel.NNCharts.chartData.dataSource.dataset.map((elm: any) => {
          return {
            text: elm.seriesname,
            value: elm.seriesname,
          };
        })
      );
    }
  });

  useEffect(() => {
    if (modelViewModel.modelResults) {
      const dataChart = modelViewModel.transformHistogramData();

      const fusionTable = new FusionCharts.DataStore().createDataTable(
        dataChart,
        [
          {
            name: "Etiqueta",
            type: "string",
          },
          {
            name: "Time",
            type: "date",
            format: "%-m/%-d/%Y",
          },
          {
            name: "Medidores",
            type: "number",
          },
        ]
      );
      const timeseriesDsSpread: any = Object.assign({}, timeseriesDs);
      timeseriesDsSpread.dataSource.data = fusionTable;

      settimeseriesDs(timeseriesDsSpread);
    }
    // // return () => {
    // //   cleanup
    // }
  }, [modelViewModel.modelResults]);

  const handleSelectChange = (value: Array<any>) => {
    let chartData = { ...modelViewModel.NNCharts.chartData };
    const dataFilter = chartData.dataSource.dataset
      .map((elm: any) => {
        return { ...elm };
      })
      .filter((elm: any) => {
        return value.indexOf(elm.seriesname) >= 0;
      });
    chartData = {
      ...modelViewModel.NNCharts.chartData,
      dataSource: {
        ...modelViewModel.NNCharts.chartData.dataSource,
        dataset: dataFilter,
      },
    };
    settargetChartData(chartData);
  };

  return (
    <MDBRow className="mt-4">
      {modelViewModel.NNCharts?.performanceMetrics?.ACC_test && (
        <MDBCol size="3">
          <MDBCard className="w-100">
            <MDBCardBody>
              <MDBCardTitle>ACC test</MDBCardTitle>
              <MDBCardText>
                {modelViewModel.NNCharts?.performanceMetrics?.ACC_test}
              </MDBCardText>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      )}
      {modelViewModel.NNCharts?.performanceMetrics?.Fscore && (
        <MDBCol size="3">
          <MDBCard className="w-100">
            <MDBCardBody>
              <MDBCardTitle>Fscore</MDBCardTitle>
              <MDBCardText>
                {modelViewModel.NNCharts?.performanceMetrics?.Fscore}
              </MDBCardText>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      )}
      {modelViewModel.NNCharts?.performanceMetrics?.UAR_SP && (
        <MDBCol size="3">
          <MDBCard className="w-100">
            <MDBCardBody>
              <MDBCardTitle>UAR_SP</MDBCardTitle>
              <MDBCardText>
                {modelViewModel.NNCharts?.performanceMetrics?.UAR_SP}
              </MDBCardText>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      )}
      {modelViewModel.NNCharts?.performanceMetrics?.UAR_dev && (
        <MDBCol size="3">
          <MDBCard className="w-100">
            <MDBCardBody>
              <MDBCardTitle>UAR_dev</MDBCardTitle>
              <MDBCardText>
                {modelViewModel.NNCharts?.performanceMetrics?.UAR_dev}
              </MDBCardText>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      )}
      {modelViewModel.NNCharts?.performanceMetrics?.D && (
        <MDBCol size="3">
          <MDBCard className="w-100">
            <MDBCardBody>
              <MDBCardTitle>D</MDBCardTitle>
              <MDBCardText>
                {modelViewModel.NNCharts?.performanceMetrics?.D}
              </MDBCardText>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      )}
      {modelViewModel.NNCharts?.performanceMetrics?.N && (
        <MDBCol size="3">
          <MDBCard className="w-100">
            <MDBCardBody>
              <MDBCardTitle>N</MDBCardTitle>
              <MDBCardText>
                {modelViewModel.NNCharts?.performanceMetrics?.N}
              </MDBCardText>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      )}

      <MDBCol size="12" className="mx-auto mt-5">
        <MDBSelect
          multiple
          search
          options={meterSelect}
          selected="Escoge los medidores a evaluar"
          selectAll
          getValue={(value) => handleSelectChange(value)}
        />
      </MDBCol>

      <div className="col-12 p-3" style={{ height: 700 }}>
        <div className="w-100 h-100">
          <ReactFC {...targetChartData} />
        </div>
      </div>

      <MDBCol>
        <div>
          {timeseriesDs.dataSource.data ? (
            <ReactFC {...timeseriesDs} />
          ) : (
            "loading"
          )}
        </div>
      </MDBCol>
    </MDBRow>
  );
}
