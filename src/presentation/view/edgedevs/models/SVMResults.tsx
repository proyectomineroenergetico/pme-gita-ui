import React, { useEffect } from 'react'
import { ModelViewModel } from '../../../view-model/model.view-model'
import { MDBRow } from 'mdbreact';
import FusionCharts from 'fusioncharts';
import Charts from 'fusioncharts/fusioncharts.charts';
import ReactFC from 'react-fusioncharts';
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';

import * as HeatMap from 'fusioncharts/fusioncharts.powercharts';

ReactFC.fcRoot(FusionCharts, Charts,HeatMap, FusionTheme);

type KMeansResultProps={
  modelViewModel:ModelViewModel
}

export default function SVMResults(props:KMeansResultProps) {
  const {modelViewModel}=props;
  return (
    
    <MDBRow>
      {modelViewModel?.SVMCharts?.arrayChartsData?.map((chart, index)=>{
          return (
            <div className="col-6 p-3" key={`chartSVM${index}`} style={{height:400}}>
              <div className="w-100 h-100">
                <ReactFC {...chart} />
              </div>
            </div>
          )
        })
      }
      {modelViewModel?.SVMCharts?.heatMapChartData && 
        (<div className="col-6 mx-auto p-3 overflow-auto" style={{height:800}}>
          <div className="w-100 h-100">
            <ReactFC {...modelViewModel?.SVMCharts?.heatMapChartData} />
          </div>
        </div>)
      }
    </MDBRow>
  )
}
