import React, { useState, useEffect } from 'react'
import { MDBCollapseHeader, MDBIcon, MDBCollapse, MDBCardBody, MDBContainer, MDBCard, MDBProgress, MDBListGroup, MDBListGroupItem, MDBBtn } from 'mdbreact'
import "../edgedevs.component.css";
import { ModelViewModel } from '../../../view-model/model.view-model';
import { format } from 'date-fns';
import {  utcToZonedTime } from 'date-fns-tz'
import ModelResultsModal from './ModelResults.modal';

type CollapsibleResultModelsProps={
  modelViewModel:ModelViewModel
}

export default function CollapsibleResultModels(props:CollapsibleResultModelsProps) {
  const {modelViewModel}=props;

  const [toggleCollapse, settoggleCollapse] = useState<any>({});
  const [resultModelModalOpen, setresultModelModalOpen] = useState(false)
  const [modelNameSelected, setmodelNameSelected] = useState('')

  const [historyOfAnalyzedModels, sethistoryOfAnalyzedModels] = useState([]);
  
  const formatDate=(dateItem:string)=>{
    const date = new Date(dateItem)
    const timeZone = 'UTC';
    const zonedDate = utcToZonedTime(date, timeZone)
    return format(zonedDate, "MM/dd/yyyy hh:mm a");
  }

  useEffect(() => {
    const historyDesc=[];
    for (let index = modelViewModel.historyOfAnalyzedModels.length-1; index >=0; index--) {
      historyDesc.push(modelViewModel.historyOfAnalyzedModels[index]);
    }
    sethistoryOfAnalyzedModels(historyDesc)
    return () => {
      sethistoryOfAnalyzedModels([])
    }
  }, [modelViewModel.historyOfAnalyzedModels])

  return (
    <>
      <div className='md-accordion mt-5'>
        {historyOfAnalyzedModels.map((elm,index)=>{
          return (
            <MDBCard key={index}>
              <MDBCollapseHeader
                tagClassName='d-flex justify-content-between'
                onClick={() => settoggleCollapse({...toggleCollapse, [index]: !toggleCollapse[index]})}
              >
                {index+1}. Modelo {elm.model_name}
                <div className="d-flex align-items-center">
                  <span className="mr-3 blue-text" style={{fontSize:'0.9rem'}}>{Math.floor(elm.progress)}%</span>
                  <MDBIcon
                    icon={toggleCollapse[index] ? 'angle-up' : 'angle-down'}
                  />
                </div>
                
              </MDBCollapseHeader>
              <MDBCollapse id='collapse1' isOpen={toggleCollapse[index]}>
                <MDBCardBody>
                  <div className="d-flex align-items-center">
                    <span className="font-weight-bold mr-3">Progreso:</span>
                    <MDBProgress material value={Math.floor(elm.progress)} height="20px">
                      {Math.floor(elm.progress)}%
                    </MDBProgress>
                  </div>
                  <MDBListGroup className="mt-3" style={{ width: "100%", }}>
                    <div className="border border-light">
                      <MDBListGroupItem>
                        <div className="d-flex">
                          <div className="flex-1 font-weight-bold">
                            Mensaje
                          </div>
                          <div className="px-3 font-weight-bold" style={{width:230}}>
                            Fecha
                          </div>
                        </div>
                      </MDBListGroupItem>
                    </div>
                    
                    {elm.history.map((item:any, index:number)=>{
                      return (
                        <div key={'history'+index} className="borderInfoBackground">
                          <MDBListGroupItem>
                            <div className="d-flex">
                              <div className="border-right border-light flex-1">
                                {item.message}
                              </div>
                              <div className="px-3" style={{width:230}}>
                                {formatDate(item.initDate)}
                              </div>
                            </div>
                          </MDBListGroupItem>
                        </div>
                      )
                    })}
                      
                    </MDBListGroup>
                    {Math.floor(elm.progress) >= 100 && <div className="d-flex justify-content-end">
                      <MDBBtn onClick={()=>{setmodelNameSelected(elm.model_name); setresultModelModalOpen(true)}} className="mx-0 mt-4" color="primary">Ver resultados</MDBBtn>
                    </div>}
                    
                    

                </MDBCardBody>
              </MDBCollapse>
            </MDBCard>
          )
        })}
        {resultModelModalOpen && <ModelResultsModal   modelViewModel={modelViewModel} modelName={modelNameSelected} onCloseModal={(isOpen=> setresultModelModalOpen(isOpen))}></ModelResultsModal>}
        
        </div>
    </>
  )
}
