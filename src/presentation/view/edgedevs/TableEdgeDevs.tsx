import React, { useEffect, useState } from "react";

import {
  MDBCard,
  MDBCardBody,
  MDBIcon,
  MDBListGroup,
  MDBListGroupItem,
  MDBRow,
  MDBCol,
} from "mdbreact";
import "./edgedevs.component.css";
import { EdgeDevInteractor } from "../../../domain/interactors/use-cases/EdgeDevInteractor";
import ReactivePowerComponent from "./reactive-power.component";
import { EdgeDevViewModel } from "../../view-model/edgedevs.view-model";

type TableEdgeDevProps = {
  edgeDevsViewModel: EdgeDevViewModel;
  edgeDevInteractor: EdgeDevInteractor;
};

const TableEdgeDevs = ({ edgeDevsViewModel, edgeDevInteractor }: TableEdgeDevProps) => {
  const [currentMeterIndex, setcurrentMeterIndex] = useState(0);

  useEffect(() => {
    edgeDevInteractor.getInfoByMeter(edgeDevsViewModel.edgedevs[currentMeterIndex]);
  },[]);

  return (
    <MDBRow className="m-0 mt-3">
      <MDBCol className="p-1 p-md-3" size="12" lg="4" xl="3">
        <div className="d-flex flex-column w-100 h-100">
          <div className="w-100 px-0 pb-3">
            <div className="input-group md-form form-sm form-1 pl-0">
              <div className="input-group-prepend">
                <span className="input-group-text special-color" id="searchEdge">
                  <MDBIcon className="text-white" icon="search" />
                </span>
              </div>
              <input
                className="form-control my-0 py-1"
                type="text"
                placeholder="Buscar frontera"
                aria-label="Buscar"
              />
            </div>
          </div>
          <div className="overflow-auto listGroup-h">
            <MDBListGroup className="meterList w-100 justify-content-center">
              {edgeDevsViewModel.edgedevs?.map((elm: string, index: number) => {
                return (
                  <MDBListGroupItem
                    active={index === currentMeterIndex}
                    className=" pointer"
                    id={`${elm}_meter`}
                    hover
                    key={elm}
                    onClick={() => {
                      setcurrentMeterIndex(index);
                      edgeDevInteractor.getInfoByMeter(edgeDevsViewModel.edgedevs[index]);
                    }}
                  >
                    {elm}
                  </MDBListGroupItem>
                );
              })}
            </MDBListGroup>
          </div>
          
        </div>
      </MDBCol>
      <MDBCol className="p-1 p-md-3" size="12" lg="8" xl="9">
        <div className="w-100">
          <MDBCard className="w-100 h-100">
            <MDBCardBody className="p-1 p-md-3">
              <ReactivePowerComponent edgeDevsViewModel={edgeDevsViewModel} />
            </MDBCardBody>

            <MDBCardBody></MDBCardBody>
          </MDBCard>
        </div>
      </MDBCol>
      
      
    </MDBRow>
  );
};

export default TableEdgeDevs;
