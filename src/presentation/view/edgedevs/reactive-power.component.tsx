import React, { Component} from 'react'

import FusionCharts from 'fusioncharts';
import TimeSeries from 'fusioncharts/fusioncharts.timeseries';
import ReactFC from 'react-fusioncharts';
import { StoreState } from "../../../reducer";
import { connect } from 'react-redux';
import { createEdgeDevPowerViewModel, EdgeDevPowerViewModel } from '../../view-model/edgedevs-power.view-model';
import { EdgeDevViewModel } from '../../view-model/edgedevs.view-model';
import { createReduxEdgedevPowerGateway } from '../../../data/state/edgedevs/ReduxEdgedevsPowerGateway';
import store from '../../../store';
import { createEdgedevPowerInteractor } from '../../../domain/interactors/use-cases/EdgeDevPowerInteractor';
ReactFC.fcRoot(FusionCharts, TimeSeries);

type ReactivePowerProps = {
  edgeDevPowerViewModel?:EdgeDevPowerViewModel,
  edgeDevsViewModel:EdgeDevViewModel
};

const edgeDevPowerGateway = createReduxEdgedevPowerGateway(store.dispatch);
const edgeDevPowerInteractor = createEdgedevPowerInteractor(edgeDevPowerGateway);

const mapStateToProps = (state: StoreState): Partial<ReactivePowerProps> => {
  return {
    edgeDevPowerViewModel: createEdgeDevPowerViewModel(state.powerEdgedevsState),
  };
};


export class ReactivePowerComponent extends Component<ReactivePowerProps, any> {


  constructor(props:ReactivePowerProps){
  super(props);
    this.state={
      timeseriesDs:{
        type: 'timeseries',
        renderAt: 'container',
        width: '100%',
        height: '700',
        dataSource: {
            caption: { text: `Series de tiempo de consumo` },
            data: null,
            yAxis: [{
                plot: [{
                    value: 'Potencia (kb)'
                }]
            }]
        }
      }
    }
  }

  componentDidUpdate(prevProps:ReactivePowerProps) {
    const {edgeDevsViewModel, edgeDevPowerViewModel} = this.props;
    if(prevProps.edgeDevsViewModel.siccodeTarget !== edgeDevsViewModel.siccodeTarget){
      const timeSeries={...this.state.timeseriesDs};
      timeSeries.dataSource.caption.text=`Series de tiempo de consumo para el medidor ${this.props.edgeDevsViewModel.siccodeTarget}`
      this.setState({timeseriesDs:timeSeries});
    }
    if(prevProps.edgeDevsViewModel.edgeDevTargetData !== edgeDevsViewModel.edgeDevTargetData){
      const dataJson=JSON.parse(edgeDevsViewModel.edgeDevTargetData);
      const dataChart = edgeDevPowerViewModel.transformDataPower(edgeDevPowerInteractor,dataJson);
      const fusionTable = new FusionCharts.DataStore().createDataTable(dataChart, [
        {
          name:"Time",
          type: "date",
          format: "%-m/%-d/%Y"
        }, {
          name: "Potencia activa",
          type: "number"
        } , {
          name: "Potencia reactiva",
          type: "number"
        }
      ]);
      const timeseriesDs = Object.assign({}, this.state.timeseriesDs);
      timeseriesDs.dataSource.data = fusionTable;
      this.setState({timeseriesDs});
    }
  }

  render() {
    return (
      
      <div>
      {this.state.timeseriesDs.dataSource.data ? (
        <ReactFC {...this.state.timeseriesDs} />
      ) : (
        'loading'
      )}
    </div>
    )
  }
}

export default connect<{}, {}, Partial<ReactivePowerProps>>(mapStateToProps)(
  ReactivePowerComponent
);
