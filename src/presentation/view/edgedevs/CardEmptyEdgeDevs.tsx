import React, { Fragment, useState } from "react";
import { MDBBtn, MDBCardText, MDBCardTitle, MDBNotification } from "mdbreact";
import { EdgeDevInteractor } from "../../../domain/interactors/use-cases/EdgeDevInteractor";
import FileUploadComponent from "../common/file-upload.component";
import { EdgeDevViewModel } from "../../view-model/edgedevs.view-model";

type CardEmptyProps = {
  edgeDevInteractor: EdgeDevInteractor;
  edgeDevViewModel: EdgeDevViewModel;
};

const CardEmptyEdgeDevs = (props: CardEmptyProps) => {
  const { edgeDevInteractor } = props;
  const [isValidTheUploadedFile, setisValidTheUploadedFile] = useState(null);
  const [file, setFile] = useState(null);

  const handlerValidFile = (isValid: boolean) => {
    if (!isValid) {
    }
    setisValidTheUploadedFile(isValid);
  };

  return (
    <Fragment>
      {isValidTheUploadedFile === false && (
        <MDBNotification
          show
          fade
          bodyClassName="p-3 px-5 white-text"
          className="danger-color"
          closeClassName="white-text"
          titleClassName="danger-color-dark white-text"
          title="Error"
          message="No se puede continuar la carga con dicho archivo. Recuerda cargar un archivo válido .xls, .xlsx"
          autohide={3000}
          style={{
            position: "fixed",
            top: "10px",
            right: "10px",
            zIndex: 9999,
          }}
        />
      )}

      <MDBCardTitle>Medidores (dispositivos de borde)</MDBCardTitle>
      <MDBCardText>
        Actualmente no tienes medidores agregados. Puedes cargar un archivo a
        continuación
      </MDBCardText>
      <FileUploadComponent
        isValidTheUploadedFile={(isValid: boolean) => handlerValidFile(isValid)}
        onUpdateFile={(file: File) => setFile(file)}
        validFiles={["excel"]}
      />
      {isValidTheUploadedFile && (
        <MDBBtn
          onClick={() => edgeDevInteractor.uploadEdgeDevsByFile(file, false)}
          className="w-100 mt-3"
          color="elegant"
        >
          Cargar
        </MDBBtn>
      )}
    </Fragment>
  );
};

export default CardEmptyEdgeDevs;
