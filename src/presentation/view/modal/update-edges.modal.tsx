import React, { useState, useEffect } from 'react'
import { MDBModal, MDBModalHeader, MDBCloseIcon, MDBModalBody, MDBModalFooter, MDBRow, MDBCol, MDBBtn, MDBNotification, MDBInput } from 'mdbreact';
import FileUploadComponent from "../common/file-upload.component";
import { EdgeDevInteractor } from '../../../domain/interactors/use-cases/EdgeDevInteractor';
import { EdgeDevViewModel } from '../../view-model/edgedevs.view-model';

type UpdateModalEdgesProps={
  isModalOpen:boolean
  edgeDevInteractor: EdgeDevInteractor,
  edgeDevsViewModel: EdgeDevViewModel

}

export default function UpdateEdgesModal(props:UpdateModalEdgesProps) {
  const {isModalOpen, edgeDevInteractor, edgeDevsViewModel}=props;
  const [isModalOpenState, setisModalOpenState] = useState(false)
  const [isValidTheUploadedFile, setisValidTheUploadedFile] = useState(null);
  useEffect(() => {
    setisModalOpenState(isModalOpen);
  
  }, [isModalOpen])

  useEffect(() => {
    if(edgeDevsViewModel.responseSuccessUpdateMeters !== null){
      setisModalOpenState(!isModalOpenState)
    }
  }, [edgeDevsViewModel.responseSuccessUpdateMeters])


  const [file, setFile] = useState(null);

  const handlerValidFile = (isValid: boolean) => {
    if (!isValid) {
    }
    setisValidTheUploadedFile(isValid);
  };

  return (
    <MDBModal
      inline={false}
      overflowScroll={false}
      isOpen={isModalOpenState}
      noClickableBodyWithoutBackdrop={true}
      size="lg"
    >
      <MDBModalHeader>
        <div className="d-flex">
          <span className="flex-1">Actualizar información de medidores</span>
          <MDBCloseIcon
            onClick={() =>
              setisModalOpenState(!isModalOpenState)
            }
          />
        </div>
      </MDBModalHeader>
      <MDBModalBody>
      {isValidTheUploadedFile === false && (
        <MDBNotification
          show
          fade
          bodyClassName="p-3 px-5 white-text"
          className="danger-color"
          closeClassName="white-text"
          titleClassName="danger-color-dark white-text"
          title="Error"
          message="No se puede continuar la carga con dicho archivo. Recuerda cargar un archivo válido .xls, .xlsx"
          autohide={3000}
          style={{
            position: "fixed",
            top: "10px",
            right: "10px",
            zIndex: 9999,
          }}
        />
      )}
        <span>Escoge una acción a realizar:</span>
        <div className="d-flex">
          <div className="px-2">
            <MDBInput
            label="Eliminar-Agregar"
            type="radio"
            value="deleteMetersOption"
            checked={edgeDevsViewModel.actionInUpdateMeters==='deleteMetersOption'}
            name="updateMetersRadioName"
            id="deleteMetersOption"
            onChange={(event:any)=>edgeDevInteractor.onChangeInputValue(event.target.value, 'actionInUpdateMeters')}
            />
          </div>
          <div className="px-2">
            <MDBInput
              label="Actualizar"
              type="radio"
              value="updateMetersOption"
              checked={edgeDevsViewModel.actionInUpdateMeters==='updateMetersOption'}
              name="updateMetersRadioName"
              id="updateMetersOption"
              onChange={(event:any)=>edgeDevInteractor.onChangeInputValue(event.target.value, 'actionInUpdateMeters')}
            />
          </div> 
        </div>
        <FileUploadComponent
          isValidTheUploadedFile={(isValid: boolean) => handlerValidFile(isValid)}
          onUpdateFile={(file: File) => setFile(file)}
          validFiles={["excel"]}
        />
      </MDBModalBody>
      <MDBModalFooter>
        <MDBRow className="w-100">
          <MDBCol size="6">
            <MDBBtn
              className="w-100"
              color="elegant"
              outline
              onClick={() =>
                setisModalOpenState(!isModalOpenState)
              }
            >
              Cancelar
            </MDBBtn>
          </MDBCol>
          {isValidTheUploadedFile && (
          <MDBCol className="w-100" size="6">
            <MDBBtn
              onClick={()=> edgeDevInteractor.uploadEdgeDevsByFile(file, true, edgeDevsViewModel.actionInUpdateMeters)}
              className="w-100"
              color="elegant"
            >
              Actualizar
            </MDBBtn>
          </MDBCol>
          )}
        </MDBRow>
      </MDBModalFooter>
    </MDBModal>
  )
}
