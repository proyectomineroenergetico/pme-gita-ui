import React, { useState, useEffect } from "react";
import { MDBBtn, MDBCard, MDBCardBody, MDBIcon } from "mdbreact";
import "./common.styles.css";
import { connect, useSelector } from "react-redux";
import { RootState } from "../../../reducer";
import { createFileUploadViewModel } from "../../view-model/file-upload.view-model";
import { useLayout } from "../../../data/hooks/layout.hook";

type FileUploadProps = {
  isValidTheUploadedFile(isvalid: boolean): void;
  onUpdateFile(file: File): void;
  validFiles: Array<string>;
};

export const FileUploadComponent = (props: FileUploadProps) => {
  // Input variables
  const fileInput = React.useRef(null);
  const [file, setFile] = useState(null);
  const [fileExtension, setfileExtension] = useState(null);
  const [hoverInInputFile, sethoverInInputFile] = useState(false);

  // state & actions variables
  const { fileUploadInteractor } = useLayout();
  const fileUploadedState = useSelector(
    (state: RootState) => state.fileUploadState
  );
  const fileUploadViewModel = createFileUploadViewModel(fileUploadedState);

  useEffect(() => {
    props.onUpdateFile(file);
    props.isValidTheUploadedFile(fileUploadViewModel.isValidTheUploadedFile);
    file?.name && asignExtension();
    function asignExtension() {
      const fileName = file?.name as string;
      setfileExtension(fileName?.substring(file?.name?.lastIndexOf(".") + 1));
    }
  });

  useEffect(() => {
    return () => {
      setfileExtension(null);
      sethoverInInputFile(false);
      fileUploadViewModel.resetFileValues(fileUploadInteractor);
      setFile(null);
    };
  }, []);

  const managementFile = (evt: any, actionType: string) => {
    const file = fileUploadViewModel.fileHandler(
      evt,
      fileInput,
      fileUploadInteractor,
      actionType,
      props.validFiles
    );
    setFile(file);
  };

  const removeTargetFile = (
    evt: React.SyntheticEvent<HTMLButtonElement, Event>
  ) => {
    evt.preventDefault();
    fileUploadViewModel.resetFileValues(fileUploadInteractor);
    setFile(null);
  };

  const resetInputValue = (evt: any) => {
    evt.target.value = null;
  };

  return (
    <MDBCard
      id="drop_zone"
      onDrop={(e: DragEvent) => managementFile(e, "DROP")}
      onMouseEnter={() => sethoverInInputFile(true)}
      onMouseLeave={() => sethoverInInputFile(false)}
      className="w-100 position-relative pointer inputFileContainer"
    >
      {fileUploadViewModel.isValidTheUploadedFile !== null && hoverInInputFile && (
        <div className="rounded inputFileMask d-flex flex-column align-items-center justify-content-center">
          <h5 className="white-text mb-0 mt-4">{file.name}</h5>
          <span className="messageInputMask white-text">
            Arrastra y suelta o haz clic para actualizar archivo.
          </span>
        </div>
      )}

      {fileUploadViewModel.isValidTheUploadedFile !== null && hoverInInputFile && (
        <div className="position-absolute rmv-uploadedFile">
          <MDBBtn color="danger" onClick={(event) => removeTargetFile(event)}>
            Eliminar <MDBIcon far icon="trash-alt" className="ml-2" />
          </MDBBtn>
        </div>
      )}

      <MDBCardBody className="d-flex align-items-center justify-content-center">
        <div className="d-flex flex-column align-items-center justify-content-center">
          <div className="position-relative">
            <MDBIcon
              className="bg-fileupload-cloud-icon"
              icon={file ? "file" : "cloud-upload-alt"}
              size="5x"
            />
            {file && (
              <span className="position-absolute white-text extensionSt">
                {fileExtension}
              </span>
            )}
          </div>

          <span className="bg-fileupload-cloud-icon mt-3">
            {fileUploadViewModel.isValidTheUploadedFile === null &&
              "Arrastra y suelta un archivo aquí o haga clic en el recuadro"}
          </span>
        </div>
        <form encType="multipart/form-data">
          <input
            className="box-file"
            type="file"
            id="inputFileUpload"
            ref={fileInput}
            onChange={(e) => managementFile(e, "LOAD")}
            onClick={(e) => resetInputValue(e)}
          />
        </form>

        {/* accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" */}
      </MDBCardBody>
    </MDBCard>
  );
};

const mapDispatchToProps = {};

export default connect(null, mapDispatchToProps)(FileUploadComponent);
