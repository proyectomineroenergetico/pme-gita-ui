import React, { Fragment } from 'react';

type LoaderProps = {
  type: string;
  hint: string;
};

const LoaderComponent = (props: LoaderProps) => {
  const { type, hint } = props;

  const switchLoader = () => {
    switch (type) {
      case 'LOADER-IMG':
        return (
          // <div>

          // </div>
          <div className="w-100 h-100 d-flex flex-column align-items-center justify-content-center bg-loader">
            <img
              className="img-fluid"
              style={{ width: 100 }}
              alt="loader app"
              src={process.env.PUBLIC_URL + '/img/spinner.svg'}
            />
            <h5 className="font-weight-bold" style={{ color: '#01579b' }}>
              {hint}
            </h5>
          </div>
        );
    }
  };

  return <Fragment>{switchLoader()}</Fragment>;
};
export default LoaderComponent;
