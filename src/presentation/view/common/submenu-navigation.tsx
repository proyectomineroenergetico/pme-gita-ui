import React, { Fragment } from "react";
import { MDBBreadcrumb, MDBBreadcrumbItem } from "mdbreact";
import { IMenuItems } from "../../../domain/entity/structures/layout.structure";

interface SubmenuNavigationProps {
  menuItems: IMenuItems[];
}

export const SubmenuNavigation = (props: SubmenuNavigationProps) => {
  return (
    <Fragment>
      <MDBBreadcrumb className="z-depth-1">
        {props.menuItems.map((elm, index) => {
          return <MDBBreadcrumbItem key={index}>{elm.name}</MDBBreadcrumbItem>;
        })}
      </MDBBreadcrumb>
    </Fragment>
  );
};
