import React, { Fragment } from "react";
import { useAuth } from "../../../../data/hooks/use-auth.hook";
import { Route, Redirect } from "react-router-dom";
import LoginComponent from "./login.component";
import { AuthInteractor } from "../../../../domain/interactors/use-cases/AuthInteractor";

type LoginRedirectProps = {
  authInteractor: AuthInteractor;
};

export const LoginRedirectComponent = (
  props: LoginRedirectProps,
  ...rest: any[]
) => {
  let auth = useAuth();

  return (
    <Fragment>
      <Route {...rest}>
        {auth.userIsAuthenticated() ? (
          <Redirect to="/dashboard/edgedevs" />
        ) : (
          <LoginComponent authInteractor={props.authInteractor} />
        )}
      </Route>
    </Fragment>
  );
};
