import {
  MDBBtn,
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCol,
  MDBContainer,
  MDBInput,
  MDBNotification,
  MDBRow,
} from "mdbreact";
import React, { useEffect } from "react";
import { AuthInteractor } from "../../../../domain/interactors/use-cases/AuthInteractor";
import { StoreState } from "../../../../reducer";
import { InputErrorComponent } from "../../layout/InputError.component";
import { createUserLoginViewModel } from "../../../view-model/user-login.view-model";
import { useSelector, useDispatch } from "react-redux";
import LoaderComponent from "../../common/loader.component";
import "../../common/common.styles.css";
import { useHistory, useLocation } from "react-router-dom";
import { useAuth } from "../../../../data/hooks/use-auth.hook";
import { createRecoveryPasswordViewModel } from "../../../view-model/recovery-password.view-model";
import { resetState } from "../../../../data/state/recovery-password/recovery-password.redux.slice";

type LoginPageProps = {
  authInteractor: AuthInteractor;
};

function LoginComponent(loginProps: LoginPageProps) {
  const { authInteractor } = loginProps;
  const loginViewModel = createUserLoginViewModel(
    useSelector((state: StoreState) => state.loginState)
  );
  const recoveryPasswordViewModel = createRecoveryPasswordViewModel(
    useSelector((state: StoreState) => state.recoveryPasswordState)
  );
  let history = useHistory();
  let location = useLocation();
  let auth = useAuth();
  const dispatch = useDispatch();

  let { from }: any = location.state || { from: { pathname: "/" } };

  useEffect(() => {
    if (auth.userIsAuthenticated() && loginViewModel.successMessage === "") {
      history.push("/dashboard");
    } else if (loginViewModel.successMessage) {
      auth.signin(loginViewModel.successMessage.auth_token);
      history.replace(from);
      loginViewModel.resetLoginSuccessResponse(authInteractor);
    }
  });

  useEffect(() => {
    if(recoveryPasswordViewModel.isSuccessUpdatePassword){
      setTimeout(() => {
        dispatch(resetState())
      }, 5000);
    }
  }, [recoveryPasswordViewModel.isSuccessUpdatePassword])

  return (
    <MDBContainer className="minh-100vh d-flex align-items-center justify-content-center">
      {loginViewModel.pendingLogin && (
        <div className="position-fixed loaderFull">
          <LoaderComponent
            hint="Identificando usuario, por favor espere ..."
            type="LOADER-IMG"
          />
        </div>
      )}
      {recoveryPasswordViewModel.isSuccessUpdatePassword && (
        <MDBNotification
          show
          fade
          bodyClassName="p-3 px-5 white-text"
          className="primary-color"
          closeClassName="white-text"
          titleClassName="primary-color-dark white-text"
          title="Info"
          message={recoveryPasswordViewModel.successMessage}
          autohide={4000}
          style={{
            position: "fixed",
            top: "10px",
            right: "10px",
            zIndex: 9999,
          }}
        />
      )}
      {loginViewModel.responseError && (
        <MDBNotification
          show
          fade
          bodyClassName="p-3 px-5 white-text"
          className="danger-color"
          closeClassName="white-text"
          titleClassName="danger-color-dark white-text"
          title="Error"
          message={loginViewModel.responseError}
          autohide={3000}
          style={{
            position: "fixed",
            top: "10px",
            right: "10px",
            zIndex: 9999,
          }}
        />
      )}
      <MDBRow>
        <MDBCol size="12" md="9" xl="10" className="mx-auto">
          <MDBCard className="w-100">
            <MDBCardBody>
              <MDBCardTitle>Inicio de sesión</MDBCardTitle>
              <MDBRow>
                <MDBCol size="12" className="p-3">
                  <MDBInput
                    label="Correo electrónico"
                    background
                    type="email"
                    value={loginViewModel.emailValue}
                    onChange={(e: any) =>
                      loginViewModel.handleEmailInputValue(
                        e.target.value,
                        authInteractor
                      )
                    }
                    onBlur={(e: any) =>
                      loginViewModel.handleEmailInputValue(
                        e.target.value,
                        authInteractor
                      )
                    }
                    size="sm"
                  />
                  <InputErrorComponent
                    errorMessage={loginViewModel.emailError}
                  />
                </MDBCol>
                <MDBCol size="12" className="p-3">
                  <MDBInput
                    label="Contraseña"
                    background
                    type="password"
                    value={loginViewModel.passwordValue}
                    onChange={(e: any) =>
                      loginViewModel.handlePasswordInputValue(
                        e.target.value,
                        authInteractor
                      )
                    }
                    onBlur={(e: any) =>
                      loginViewModel.handlePasswordInputValue(
                        e.target.value,
                        authInteractor
                      )
                    }
                    size="sm"
                  />
                  <InputErrorComponent
                    errorMessage={loginViewModel.passwordError}
                  />
                </MDBCol>
                <MDBCol size="12" className="p-3">
                    <span onClick={()=>history.push("/recovery")} className="blue-text pointer">¿Has olvidado tu contraseña?</span>
                </MDBCol>
                <MDBCol size="12" className="p-3">
                  <MDBBtn
                    onClick={() =>
                      loginViewModel.handleTheSignin(authInteractor)
                    }
                    color="elegant"
                    className="w-100 m-0"
                  >
                    Iniciar sesión
                  </MDBBtn>
                </MDBCol>
                <MDBCol size="12" className="p-3">
                  <MDBBtn
                    onClick={() =>
                      history.push('signup')
                    }
                    color="elegant"
                    outline
                    className="w-100 m-0"
                  >
                    Regístrarse
                  </MDBBtn>
                </MDBCol>
              </MDBRow>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  );
}

export default LoginComponent;
