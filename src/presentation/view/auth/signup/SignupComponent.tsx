import React, { useEffect } from 'react'
import { MDBContainer, MDBRow, MDBCol, MDBCard, MDBCardBody, MDBCardTitle, MDBInput, MDBBtn, MDBNotification } from 'mdbreact'
import { SignupInteractor } from '../../../../domain/interactors/use-cases/SignupInteractor';
import { useSelector, useDispatch } from 'react-redux';
import { StoreState } from "../../../../reducer";
import { InputErrorComponent } from '../../layout/InputError.component';
import { useHistory, useLocation } from 'react-router-dom';
import { useAuth } from '../../../../data/hooks/use-auth.hook';
import LoaderComponent from '../../common/loader.component';
import { resetSignupResponseError, resetSignupState } from "../../../../data/state/signup/signup.redux.slice";


type SignupProps = {
  signupInteractor: SignupInteractor;
};

export default function SignupComponent(props:SignupProps) {
  const {signupInteractor} = props;
  const signupState = useSelector((state:StoreState) => state.signupState);
  let history = useHistory();
  let auth = useAuth();
  let location = useLocation();
  const dispatch = useDispatch();

  useEffect(() => {
    if(signupState.responseSuccess){
      let { from }: any = location.state || { from: { pathname: "/" } };
      auth.signin(signupState.responseSuccess.auth_token);
      history.replace(from);
    }
    if(signupState.responseError){
      setTimeout(() => {
        dispatch(resetSignupResponseError(undefined));  
      }, 5000);
    }
  }, [signupState])

  return (
    <MDBContainer className="minh-100vh d-flex align-items-center justify-content-center">
      {signupState.pendingSignup && (
        <div className="position-fixed loaderFull">
          <LoaderComponent
            hint="Registrando usuario, por favor espere ..."
            type="LOADER-IMG"
          />
        </div>
      )}
      {signupState.responseError !== undefined && (
          <MDBNotification
            show
            fade
            bodyClassName="p-3 px-5 white-text"
            className="danger-color"
            closeClassName="white-text"
            titleClassName="danger-color-dark white-text"
            title="Error"
            message={signupState.responseError.message}
            autohide={4000}
            style={{
              position: "fixed",
              top: "10px",
              right: "10px",
              zIndex: 9999,
            }}
          />
        )}
    <MDBRow>
      <MDBCol size="12" md="9" xl="10" className="mx-auto">
        <MDBCard className="w-100">
          <MDBCardBody>
            <MDBCardTitle>Registro de usuario</MDBCardTitle>
            <MDBRow>
              <MDBCol size="12" className="p-3">
                <MDBInput
                  label="Correo electrónico"
                  background
                  type="email"
                  value={signupState.emailValue}
                  onChange={(e: any) =>{ 
                    signupInteractor.handleEmailInputValue(e.target.value)
                  }}
                  onBlur={(e: any) =>
                    signupInteractor.handleEmailInputValue(e.target.value)
                  }
                  size="sm"
                />
                <InputErrorComponent
                  errorMessage={signupState.emailError}
                />
              </MDBCol>
              <MDBCol size="12" className="p-3">
                <MDBInput
                  label="Contraseña"
                  background
                  type="password"
                  size="sm"
                  value={signupState.passwordValue}
                  onChange={(e: any) =>{ 
                    signupInteractor.handlePasswordInputValue(e.target.value)
                  }}
                  onBlur={(e: any) =>
                    signupInteractor.handlePasswordInputValue(e.target.value)
                  }
                />
                <InputErrorComponent
                  errorMessage={signupState.passwordError}
                />
              </MDBCol>
              <MDBCol size="12" className="p-3">
                <MDBBtn
                  onClick={() => {
                    signupInteractor.signup({email:signupState.emailValue, password:signupState.passwordValue})
                  }}
                  color="elegant"
                  className="w-100 m-0"
                >
                  Registrarse
                </MDBBtn>
              </MDBCol>
              <MDBCol size="12" className="p-3">
                <MDBBtn
                  onClick={() => history.push('login')}
                  color="elegant"
                  outline
                  className="w-100 m-0"
                >
                  Ya tengo una cuenta
                </MDBBtn>
              </MDBCol>
            </MDBRow>
          </MDBCardBody>
        </MDBCard>
      </MDBCol>
    </MDBRow>
  </MDBContainer>
  )
}
