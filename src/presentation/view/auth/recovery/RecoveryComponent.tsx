import React, { useState, useEffect } from 'react'
import { MDBContainer, MDBCard, MDBCardBody, MDBCardText, MDBInput, MDBRow, MDBCol, MDBBtn } from 'mdbreact'
import { createUserLoginViewModel } from '../../../view-model/user-login.view-model';
import { StoreState } from '../../../../reducer';
import { useSelector, useDispatch } from 'react-redux';
import { createAuthInteractor } from '../../../../domain/interactors/use-cases/AuthInteractor';
import { createReduxAuthGateway } from '../../../../data/state/auth/ReduxAuthGateway';
import store from '../../../../store';
import { InputErrorComponent } from '../../layout/InputError.component';
import { createReduxRecoveryPasswordGateway } from '../../../../data/state/recovery-password/ReduxRecoveryPasswordGateway';
import { createRecoveryPasswordInteractor } from '../../../../domain/interactors/use-cases/RecoveryPasswordInteractor';
import { createRecoveryPasswordViewModel } from '../../../view-model/recovery-password.view-model';
import { resetState, recoveryUser } from '../../../../data/state/recovery-password/recovery-password.redux.slice';
import LoaderComponent from '../../common/loader.component';
import { useHistory } from 'react-router-dom';

export default function RecoveryComponent() {
  const recoveryPasswordViewModel = createRecoveryPasswordViewModel(
    useSelector((state: StoreState) => state.recoveryPasswordState)
  );
  const recoveryGateway = createReduxRecoveryPasswordGateway(store.dispatch);
  const recoveryPasswordInteractor = createRecoveryPasswordInteractor(recoveryGateway);
  const dispatch = useDispatch();
  const [emailValue, setemailValue] = useState('');
  const [sendEmailAction, setsendEmailAction] = useState(false);
  let history = useHistory();

  useEffect(() => {
    if(recoveryPasswordViewModel.emailError===''&& sendEmailAction){
      // console.log(recoveryPasswordViewModel.emailError, 'espor que esta bien');
      dispatch(recoveryUser(emailValue))
    }
  }, [recoveryPasswordViewModel.emailError, sendEmailAction])

  useEffect(() => {
    return () => {
      dispatch(resetState())
    }
  }, [])

  const sendRecoveryPasswordEmail=()=>{
    
  }

  return (
    <main className="w-100 h-100 grey lighten-4 minh-100vh align-items-center d-flex">
      {recoveryPasswordViewModel.pendingRecovery && (
        <div className="position-fixed loaderFull">
          <LoaderComponent
            hint="Enviando correo electrónico de recuperación, por favor espere ..."
            type="LOADER-IMG"
          />
        </div>
      )}
      <MDBCard className="col-4 mx-auto">
        <MDBCardBody>
          {
            !recoveryPasswordViewModel.isSuccessEmailSend && 
              <>
                <MDBCardText className="black-text"><strong>Ingrese la dirección de correo electrónico verificada de su cuenta de usuario y le enviaremos un enlace para restablecer la contraseña.</strong></MDBCardText>
                <MDBRow>
                  <MDBCol size="12" className="p-3">
                    <MDBInput
                      label="Correo electrónico"
                      background
                      type="email"
                      value={emailValue}
                      onChange={(e: any) =>{
                        setemailValue(e.target.value);
                        recoveryPasswordInteractor.handleEmailRecovery(e.target.value);
                        setsendEmailAction(false);
                      }}
                      onBlur={(e: any) =>{
                        recoveryPasswordInteractor.handleEmailRecovery(e.target.value);
                        setsendEmailAction(false);
                      }}
                      size="sm"
                    />
                    {recoveryPasswordViewModel.emailError!=='' &&  <InputErrorComponent
                      errorMessage={recoveryPasswordViewModel.emailError}
                    />}
                  </MDBCol>
                  <MDBCol>
                    <MDBBtn
                      onClick={() =>{
                        recoveryPasswordInteractor.handleEmailRecovery(emailValue);
                        setsendEmailAction(true);
                      }}
                      color="elegant"
                      className="w-100 m-0"
                    >
                      Enviar correo electrónico de restablecimiento de contraseña
                    </MDBBtn>
                  </MDBCol>
                </MDBRow>
              </>
          }
          {
            recoveryPasswordViewModel.isSuccessEmailSend && 
            <>
              <MDBRow>
                <MDBCol size="12" className="mb-3">
                  <MDBCardText>{recoveryPasswordViewModel.successMessage}</MDBCardText>
                </MDBCol>
                <MDBCol>
                    <MDBBtn
                      onClick={() =>{
                        history.goBack()
                      }}
                      color="elegant"
                      className="w-100 m-0"
                    >
                      OK
                    </MDBBtn>
                  </MDBCol>
              </MDBRow>
            </>
          }
          
        </MDBCardBody>
      </MDBCard>
    </main>
  )
}
