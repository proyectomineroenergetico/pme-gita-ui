import React, { useState, useEffect } from 'react'
import { MDBCard, MDBCardBody, MDBRow, MDBCol, MDBInput, MDBBtn, MDBNotification, MDBCardText } from 'mdbreact';
import { createRecoveryPasswordViewModel } from '../../../view-model/recovery-password.view-model';
import { StoreState } from '../../../../reducer';
import { useSelector, useDispatch } from 'react-redux';
import { createReduxRecoveryPasswordGateway } from '../../../../data/state/recovery-password/ReduxRecoveryPasswordGateway';
import store from '../../../../store';
import { createRecoveryPasswordInteractor } from '../../../../domain/interactors/use-cases/RecoveryPasswordInteractor';
import { InputErrorComponent } from '../../layout/InputError.component';
import { setresponseError } from '../../../../data/state/recovery-password/recovery-password.redux.slice';
import { useLocation, useHistory } from 'react-router-dom';

export default function ResetPasswordComponent(props:any) {
  function useQuery() {
    return new URLSearchParams(useLocation().search);
  }

  const dispatch = useDispatch();
  let query = useQuery();
  let history = useHistory();
  const [password, setpassword] = useState('');
  const [confirmPassword, setconfirmPassword] = useState('');
  const [resetPasswordAction, setResetPasswordAction] = useState(false);
  const recoveryPasswordViewModel = createRecoveryPasswordViewModel(
    useSelector((state: StoreState) => state.recoveryPasswordState)
  );
  const recoveryGateway = createReduxRecoveryPasswordGateway(store.dispatch);
  const recoveryPasswordInteractor = createRecoveryPasswordInteractor(recoveryGateway);
  const [showNotification, setshowNotification] = useState(false);
  const [errorValidation, seterrorValidation] = useState('');
  const [errorToken, seterrorToken] = useState('');

  // useEffect(() => {
  //   console.log(recoveryPasswordViewModel.responseError)
  //   let isMounted = true;
  //   if(recoveryPasswordViewModel.responseError!=='' && isMounted){
  //     setshowNotification(true)
  //   }
  //   return () => { isMounted = false };
  // },)

  useEffect(() => {

    if(recoveryPasswordViewModel.isSuccessUpdatePassword){
      history.push('login');
    }
    let isValidToken=recoveryPasswordInteractor.getDecodeResetToken(query.get('user')).isValid;
    if(!isValidToken){
      seterrorToken('El token ingresado esta mal formado, o no tienes permiso para acceder a este recurso');
      setTimeout(() => {
        history.push('login')  
      }, 4000);
      
    }
    let isMounted=true;
    let timeTask:NodeJS.Timeout;
    if(recoveryPasswordViewModel.responseError!==''){
      if(isMounted){
        seterrorValidation(recoveryPasswordViewModel.responseError)
        timeTask=setTimeout(() => {
          
            dispatch(setresponseError(''))  
          
          
        }, 4000);
      }
    } 
    return()=> {
      seterrorValidation('');
      clearTimeout(timeTask);
      isMounted=false;
    }
    
    // let isMounted = true;
    // if(recoveryPasswordViewModel.responseError!=='' && isMounted){
    //   setshowNotification(true)
    // }
    // return () => { isMounted = false };
  },[recoveryPasswordViewModel.responseError, recoveryPasswordViewModel.isSuccessUpdatePassword])

  return (
    <main className="w-100 h-100 grey lighten-4 minh-100vh align-items-center d-flex">
      {errorToken!=='' && (
        <MDBNotification
          show
          fade
          bodyClassName="p-3 px-5 white-text"
          className="danger-color"
          closeClassName="white-text"
          titleClassName="danger-color-dark white-text"
          title="Error"
          message={errorToken}
          autohide={4000}
          style={{
            position: "fixed",
            top: "10px",
            right: "10px",
            zIndex: 9999,
          }}
        />
      )}
            
      { !errorToken && 
      <MDBCard className="col-4 mx-auto">
        <MDBCardBody>
          <MDBRow>
            {errorValidation!=='' && (
              <MDBNotification
                show
                fade
                bodyClassName="p-3 px-5 white-text"
                className="danger-color"
                closeClassName="white-text"
                titleClassName="danger-color-dark white-text"
                title="Error"
                message={errorValidation}
                autohide={4000}
                style={{
                  position: "fixed",
                  top: "10px",
                  right: "10px",
                  zIndex: 9999,
                }}
              />
            )}
            <MDBCardText className="black-text mx-auto"><strong>Ingresa la nueva contraseña que deseas configurar.</strong></MDBCardText>

            <MDBCol size="12" className="p-3">
              <MDBInput
                label="Contraseña"
                background
                type="password"
                value={password}
                onChange={(e: any) =>{
                  setpassword(e.target.value);
                  recoveryPasswordInteractor.handleResetPasswordValue(e.target.value);
                  setResetPasswordAction(false);
                }}
                onBlur={(e: any) =>{
                  recoveryPasswordInteractor.handleResetPasswordValue(e.target.value);
                  setResetPasswordAction(false);
                }}
                size="sm"
              />
              {recoveryPasswordViewModel.passwordError!=='' &&  <InputErrorComponent
                errorMessage={recoveryPasswordViewModel.passwordError}
              />}
            </MDBCol>
            <MDBCol size="12" className="p-3">
              <MDBInput
                label="Confirmar nueva contraseña"
                background
                type="password"
                value={confirmPassword}
                onChange={(e: any) =>{
                  setconfirmPassword(e.target.value);
                  recoveryPasswordInteractor.handleResetConfirmPasswordValue(e.target.value);
                  setResetPasswordAction(false);
                }}
                onBlur={(e: any) =>{
                  recoveryPasswordInteractor.handleResetConfirmPasswordValue(e.target.value);
                  setResetPasswordAction(false);
                }}
                size="sm"
              />
              {recoveryPasswordViewModel.confirmPasswordError!=='' &&  <InputErrorComponent
                errorMessage={recoveryPasswordViewModel.confirmPasswordError}
              />}
            </MDBCol>
            <MDBCol size="12" className="p-2">
              <MDBBtn
                onClick={() =>{
                  setshowNotification(false);
                  recoveryPasswordInteractor.handleResetPasswordAction(password, confirmPassword, query.get('user'));
                  setResetPasswordAction(true);
                }}
                color="elegant"
                className="w-100 m-0"
              >
                Restablecer contraseña
              </MDBBtn>
            </MDBCol>
            <MDBCol size="12" className="p-2">
              <MDBBtn
                onClick={() =>{
                  history.push('recovery')
                }}
                color="elegant"
                outline
                className="w-100 m-0"
              >
                Volver a enviar enlace de recuperación
              </MDBBtn>
            </MDBCol>
        </MDBRow>
      </MDBCardBody>
    </MDBCard>
    }
  </main>
  )
}
