import React, { Fragment } from "react";
import { Redirect, Route } from "react-router-dom";
import { useAuth } from "../../../data/hooks/use-auth.hook";

const PrivateRoute = (children: any, ...rest: any[]) => {
  let auth = useAuth();

  return (
    <Fragment>
      <Route
        {...rest}
        render={({ location }: any) =>
          auth.userIsAuthenticated() ? (
            children.children
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: location },
              }}
            />
          )
        }
      />
    </Fragment>
  );
};

export default PrivateRoute;
