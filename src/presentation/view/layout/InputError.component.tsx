import React from "react";

import "./layout.styles.css";

type InputErrorProps = {
  errorMessage: string;
  className?: string;
};

export const InputErrorComponent = (props: InputErrorProps) => {
  const { errorMessage, className } = props;
  return <span className={className || "inputErrorLabel"}>{errorMessage}</span>;
};
