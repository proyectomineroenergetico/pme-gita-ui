import {
  MDBCollapse,
  MDBContainer,
  MDBDropdown,
  MDBDropdownItem,
  MDBDropdownMenu,
  MDBDropdownToggle,
  MDBHamburgerToggler,
  MDBIcon,
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavItem,
  MDBNavLink,
  MDBNotification,
} from "mdbreact";
import React, { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
  useHistory,
  Link,
} from "react-router-dom";
import { createReduxEdgedevGateway } from "../../../data/state/edgedevs/ReduxEdgedevsGateway";
import EdgeDevEntity from "../../../domain/entity/models/edgedevs.entity";
import { createEdgedevInteractor } from "../../../domain/interactors/use-cases/EdgeDevInteractor";
import store from "../../../store";
import EdgedevsComponent from "../edgedevs/edgedevs.component";
import "./layout.styles.css";
import { useAuth } from "../../../data/hooks/use-auth.hook";
import { useLayout } from "../../../data/hooks/layout.hook";
import { IMenuItems } from "../../../domain/entity/structures/layout.structure";
import { StoreState } from "../../../reducer";
import { useSelector, useDispatch } from "react-redux";
import { resetSignupState } from "../../../data/state/signup/signup.redux.slice";
import { logoutUser } from "../../../data/state/auth/auth.redux.slice";

const edgedevGateway = createReduxEdgedevGateway(store.dispatch);
const edgeDevInteractor = createEdgedevInteractor(edgedevGateway);
const edgeDevEntity = new EdgeDevEntity();

const AppNavigationComponent = () => {
  const [isNotCollapsedMenu, setisNotCollapsedMenu] = useState(false);

  const auth = useAuth();
  const history = useHistory();
  const layout = useLayout();
  const dispatch = useDispatch();

  const logout = () => {
    dispatch(logoutUser(""));
    auth.signout();
    history.replace("/");
  };

  const signupState = useSelector((state: StoreState) => state.signupState);

  useEffect(() => {
    if (signupState.responseSuccess) {
      setTimeout(() => {
        dispatch(resetSignupState());
      }, 5000);
    }
  }, []);

  return (
    <Router>
      {signupState.responseSuccess !== undefined && (
        <MDBNotification
          show
          fade
          bodyClassName="p-3 px-5 white-text"
          className="success-color"
          closeClassName="white-text"
          titleClassName="success-color-dark white-text"
          title="Usuario registrado"
          message={signupState.responseSuccess.message}
          autohide={4000}
          style={{
            position: "fixed",
            top: "10px",
            right: "10px",
            zIndex: 9999,
          }}
        />
      )}
      <div className="d-flex flex-md-row flex-column minh-100vh">
        <div
          className={
            "slimSideNav stylish-color position-fixed h-100 d-none d-md-flex flex-column " +
            (isNotCollapsedMenu ? "slimWidthMax" : "")
          }
        >
          <div className="d-flex p-2 w-100 align-items-center">
            {/* <img
              alt=""
              src="https://mdbootstrap.com/img/Photos/Avatars/img%20(10).jpg"
              className="rounded-circle d-avatar mr-3"
            /> */}
            <div
              className={
                "rounded-circle unique-color-dark d-flex align-items-center justify-content-center " +
                (isNotCollapsedMenu ? "mr-3" : "")
              }
              style={{
                width: 50,
                height: 50,
                color: "#3F729B",
              }}
            >
              <span
                style={{
                  fontSize: 20,
                  fontWeight: 700,
                  textTransform: "uppercase",
                }}
              >
                {layout.username.charAt(0)}
              </span>
            </div>
            {isNotCollapsedMenu && (
              <span className="white-text username-label">
                {layout.username}
              </span>
            )}
          </div>
          <hr className="" />
          <div className="d-flex flex-1 overflow-hidden">
            <ul className="list-group w-100">
              {layout.menu_items.map((menuItem: IMenuItems, index: number) => {
                return (
                  <Link
                    key={`desktopMenuItem${index}`}
                    to={menuItem.redirectTo}
                  >
                    <li className="p-2 font-style-menuli pointer">
                      <div
                        className={
                          "d-flex align-items-center " +
                          (isNotCollapsedMenu ? "" : "justify-content-center")
                        }
                      >
                        <MDBIcon
                          icon={menuItem.menuIcon}
                          className={isNotCollapsedMenu ? "mr-3" : "m-0"}
                        />
                        <span className={isNotCollapsedMenu ? "" : "d-none"}>
                          {menuItem.name}
                        </span>
                      </div>
                    </li>
                  </Link>
                );
              })}
              <li
                className="p-2 font-style-menuli pointer"
                onClick={() => logout()}
              >
                <div
                  className={
                    "d-flex align-items-center " +
                    (isNotCollapsedMenu ? "" : "justify-content-center")
                  }
                >
                  <MDBIcon
                    icon="power-off"
                    className={isNotCollapsedMenu ? "mr-3" : "m-0"}
                  />
                  <span className={isNotCollapsedMenu ? "" : "d-none"}>
                    Cerrar sesión
                  </span>
                </div>
              </li>
            </ul>
          </div>
          <div
            onClick={() => setisNotCollapsedMenu(!isNotCollapsedMenu)}
            className={
              "d-flex align-items-center mb-3 pointer " +
              (isNotCollapsedMenu ? "" : "justify-content-center")
            }
          >
            {isNotCollapsedMenu ? (
              <div className="d-flex boneText align-items-center ml-2">
                <MDBIcon icon="angle-double-left" className="mr-3" />
                Minimizar menú
              </div>
            ) : (
              <MDBIcon icon="angle-double-right boneText" />
            )}
          </div>
        </div>
        <MDBNavbar fixed="top" className="d-md-none" color="stylish-color" dark>
          <MDBContainer>
            <MDBNavbarBrand>Proyecto minero energético</MDBNavbarBrand>
            <MDBHamburgerToggler
              color="#F5F5F5"
              id="hamburger1"
              onClick={() => setisNotCollapsedMenu(!isNotCollapsedMenu)}
            />
            <MDBCollapse isOpen={isNotCollapsedMenu} navbar>
              <MDBNavbarNav left>
                {layout.menu_items.map(
                  (menuItem: IMenuItems, index: number) => {
                    return (
                      <MDBNavItem key={`desktopMenuItem${index}`}>
                        <MDBNavLink to={menuItem.redirectTo}>
                          <MDBIcon className="mr-3" icon={menuItem.menuIcon} />
                          {menuItem.name}
                        </MDBNavLink>
                      </MDBNavItem>
                    );
                  }
                )}
                <MDBNavItem onClick={() => logout()}>
                  <MDBNavLink to="#!">
                    <MDBIcon icon="power-off" className="mr-3" />
                    Cerrar sesión
                  </MDBNavLink>
                </MDBNavItem>
              </MDBNavbarNav>
            </MDBCollapse>
          </MDBContainer>
        </MDBNavbar>
        <main
          className={
            isNotCollapsedMenu
              ? "dashboard-content d-flex flex-column margin-sidenav-full flex-1"
              : "dashboard-content d-flex flex-column margin-sidenav-sm flex-1"
          }
        >
          <MDBNavbar
            double
            expand="md"
            scrolling
            color="grey lighten-5"
            dark
            className="position-sticky sticky-header d-md-flex d-none p-2"
          >
            <MDBNavbarBrand>
              <strong className="textGrey ml-3">
                Proyecto minero energético
              </strong>
            </MDBNavbarBrand>
            <MDBCollapse id="navbarCollapse3" navbar>
              <MDBNavbarNav left></MDBNavbarNav>
              <MDBNavbarNav right>
                <MDBNavItem>
                  <MDBDropdown dropleft>
                    <MDBDropdownToggle nav caret>
                      <MDBIcon icon="user" className="textGrey" />
                    </MDBDropdownToggle>
                    <MDBDropdownMenu className="">
                      <MDBDropdownItem>Perfil</MDBDropdownItem>
                    </MDBDropdownMenu>
                  </MDBDropdown>
                </MDBNavItem>
              </MDBNavbarNav>
            </MDBCollapse>
          </MDBNavbar>
          <div
            style={{ paddingBottom: 150 }}
            className="px-3 pt-3 flex-1 grey lighten-4 mt-md-0"
          >
            <Switch>
              <Route exact path="/dashboard">
                <Redirect to="/dashboard/edgedevs" />
              </Route>
              <Route path="/dashboard/edgedevs">
                <EdgedevsComponent
                  edgeDevInteractor={edgeDevInteractor}
                  edgeDevEntity={edgeDevEntity}
                />
              </Route>
            </Switch>
          </div>
        </main>
      </div>
    </Router>
  );
};

export default AppNavigationComponent;
