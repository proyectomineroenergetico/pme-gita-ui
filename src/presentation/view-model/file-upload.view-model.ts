import { FileUploadedState } from "../../domain/entity/structures/layout.structure";
import { FileUploadInteractor } from "../../domain/interactors/use-cases/FileUploadInteractor";

export interface FileUploadViewModel {
  isValidTheUploadedFile: boolean;
  fileHandler(
    evt: any,
    fileInputRef: React.MutableRefObject<any>,
    fileUploadInteractor: FileUploadInteractor,
    actionType: string,
    whatAreTheValidFileType: Array<string>
  ): File;
  resetFileValues(fileUploadInteractor: FileUploadInteractor): void;
}

export function createFileUploadViewModel(
  fileUploadedState: FileUploadedState
): FileUploadViewModel {
  return {
    fileHandler(
      evt: any,
      fileInputRef: React.MutableRefObject<any>,
      fileUploadInteractor: FileUploadInteractor,
      actionType: string,
      whatAreTheValidFileType: Array<string>
    ) {
      evt.preventDefault();
      const file =
        actionType === "DROP"
          ? evt.dataTransfer.files[0]
          : fileInputRef.current.files[0];
      const type =
        actionType === "DROP"
          ? evt.dataTransfer.files[0].type
          : fileInputRef.current.files[0].type;
      fileUploadInteractor.managementFile(type, whatAreTheValidFileType);
      return file;
    },
    resetFileValues(fileUploadInteractor: FileUploadInteractor) {
      fileUploadInteractor.resetFileValues();
    },
    get isValidTheUploadedFile() {
      return fileUploadedState.isValidTheUploadedFile;
    },
  };
}
