import { EdgeDevState } from "../../domain/entity/structures/edgedevs.structures";
import { IMenuItems } from "../../domain/entity/structures/layout.structure";
import { EdgeDevInteractor } from "../../domain/interactors/use-cases/EdgeDevInteractor";

export interface EdgeDevViewModel {
  edgedevs: Array<string>;
  menuItemsForEdge: IMenuItems[];
  isLoadingFile: boolean;
  loaderMessage: string;
  responseError: string;
  responseSuccess: string;
  responseSuccessUpdateMeters: string;
  characteristicsModalOpen: boolean;
  edgeDevTargetData:string;
  siccodeTarget:string;
  actionInUpdateMeters:string;
  updateEdgesModalIsOpen:boolean;
  changeModalUpdateMeterState(edgeDevInteractor: EdgeDevInteractor):void;
  getEdgeDevsList(edgeDevInteractor: EdgeDevInteractor): void;
  toggleModal(
    edgeDevInteractor: EdgeDevInteractor,
    key: string,
    value: boolean
  ): void;
}

export function createEdgeDevViewModel(
  edgeDevState: EdgeDevState
): EdgeDevViewModel {
  return {
    get edgedevs(): Array<string> {
      return edgeDevState.edgedevs;
    },

    get actionInUpdateMeters():string{
      return edgeDevState.actionInUpdateMeters;
    },

    get menuItemsForEdge(): IMenuItems[] {
      return edgeDevState.menuItemsForEdge;
    },

    get isLoadingFile(): boolean {
      return edgeDevState.isLoaderShown;
    },

    get loaderMessage(): string {
      return edgeDevState.loaderMessage;
    },

    get responseError(): string {
      return edgeDevState.responseError;
    },

    get responseSuccess(): string {
      return edgeDevState.responseSuccess;
    },

    get responseSuccessUpdateMeters(): string{
      return edgeDevState.responseSuccessUpdateMeters;
    },

    get characteristicsModalOpen(): boolean {
      return edgeDevState.characteristicsModalOpen;
    },

    get edgeDevTargetData():string{
      return edgeDevState.edgeDevTargetData
    },

    get siccodeTarget():string{
      return edgeDevState.siccodeTarget
    },

    get updateEdgesModalIsOpen():boolean{
      return edgeDevState.updateEdgesModalIsOpen;
    },

    getEdgeDevsList(edgeDevInteractor: EdgeDevInteractor) {
      return edgeDevInteractor.getEdgeDevicesList();
    },

    toggleModal(
      edgeDevInteractor: EdgeDevInteractor,
      key: string,
      value: boolean
    ) {
      edgeDevInteractor.toogleModal(key, value);
    },

    changeModalUpdateMeterState(edgeDevInteractor: EdgeDevInteractor):void{
      edgeDevInteractor.changeModalUpdateMeterState(this.updateEdgesModalIsOpen)
    }
  };
}
