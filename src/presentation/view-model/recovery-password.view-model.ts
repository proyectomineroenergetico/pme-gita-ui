import { RecoveryPasswordState } from "../../domain/entity/structures/user.structure";

export interface RecoveryPasswordViewModel {
  emailError:string;
  passwordError:string;
  confirmPasswordError:string;
  responseError:string;
  isSuccessEmailSend:boolean;
  pendingRecovery:boolean;
  isSuccessUpdatePassword:boolean;
  successMessage:string;
}

export function createRecoveryPasswordViewModel(
  recoveryPasswordState: RecoveryPasswordState
): RecoveryPasswordViewModel {
  return {
    get emailError(): string {
      return recoveryPasswordState.emailError;
    },
    get passwordError(): string {
      return recoveryPasswordState.passwordError;
    },
    get confirmPasswordError(): string {
      return recoveryPasswordState.confirmPasswordError;
    },
    get responseError(): string {
      return recoveryPasswordState.responseError;
    },

    get isSuccessEmailSend():boolean{
      return recoveryPasswordState.isSuccessEmailSend
    },

    get pendingRecovery():boolean{
      return recoveryPasswordState.pendingRecovery
    },

    get successMessage():string{
      return recoveryPasswordState.successMessage
    },

    get isSuccessUpdatePassword():boolean{
      return recoveryPasswordState.isSuccessUpdatePassword
    }
  };
}
