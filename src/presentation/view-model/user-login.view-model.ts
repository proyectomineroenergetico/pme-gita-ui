import { AuthInteractor } from "../../domain/interactors/use-cases/AuthInteractor";
import { LoginState } from "../../domain/entity/structures/user.structure";

export interface UserLoginViewModel {
  emailError: string;
  passwordError: string;
  emailValue: string;
  passwordValue: string;
  pendingLogin: boolean;
  responseError: string;
  successMessage: any;
  handlePasswordInputValue(value: string, authInteractor: AuthInteractor): void;
  handleEmailInputValue(value: string, authInteractor: AuthInteractor): void;
  handleTheSignin(authInteractor: AuthInteractor): void;
  resetLoginSuccessResponse(authInteractor: AuthInteractor): void;
}

export function createUserLoginViewModel(
  loginState: LoginState
): UserLoginViewModel {
  return {
    get emailError(): string {
      return loginState.emailError;
    },
    get passwordError(): string {
      return loginState.passwordError;
    },
    get emailValue(): string {
      return loginState.emailValue;
    },
    get passwordValue(): string {
      return loginState.passwordValue;
    },
    get pendingLogin(): boolean {
      return loginState.pendingLogin;
    },
    get responseError(): string {
      return loginState.responseError;
    },
    get successMessage(): any {
      return loginState.successMessage;
    },
    resetLoginSuccessResponse(authInteractor: AuthInteractor) {
      authInteractor.resetLoginSuccessResponse();
    },
    handlePasswordInputValue(value: string, authInteractor: AuthInteractor) {
      authInteractor.handlePasswordInputValue(value);
    },
    handleEmailInputValue(value: string, authInteractor: AuthInteractor) {
      authInteractor.handleEmailInputValue(value);
    },
    handleTheSignin(authInteractor: AuthInteractor) {
      this.responseError &&
        this.responseError !== "" &&
        authInteractor.resetResponseError();
      if (
        this.emailError === null ||
        this.emailError !== "" ||
        this.passwordError === null ||
        this.passwordError !== ""
      ) {
        this.handleEmailInputValue(this.emailValue, authInteractor);
        this.handlePasswordInputValue(this.passwordValue, authInteractor);
      } else {
        authInteractor.loginUser({
          email: this.emailValue,
          password: this.passwordValue,
        });
      }
    },
  };
}
