import { EdgedevPowerState, FusionChartSchema } from "../../domain/entity/structures/stats.structure";
import { EdgeDevPowerInteractor } from "../../domain/interactors/use-cases/EdgeDevPowerInteractor";
import { EdgeDevTargetData } from "../../domain/entity/structures/edgedevs.structures";

export interface EdgeDevPowerViewModel {
  activePowerSchema:FusionChartSchema[],
  timeSeriesDs:any;
  powerDataEdgeDev:Array<Array<string | number>>;
  transformDataPower(edgeDevPowerInteractor: EdgeDevPowerInteractor, data:EdgeDevTargetData):Array<Array<number | string>>;
}

export function createEdgeDevPowerViewModel(
  edgeDevPowerState: EdgedevPowerState
): EdgeDevPowerViewModel {
  return {
    get activePowerSchema():FusionChartSchema[]{
      return edgeDevPowerState.activePowerSchema
    },

    get timeSeriesDs():any{
      return edgeDevPowerState.timeSeriesDs
    },

    get powerDataEdgeDev():Array<Array<string | number>>{
      return edgeDevPowerState.powerDataEdgeDev
    },

    transformDataPower(edgeDevPowerInteractor: EdgeDevPowerInteractor, data:EdgeDevTargetData):Array<Array<number | string>>{
      return edgeDevPowerInteractor.transformDataPower(data)
    }
  };
}
