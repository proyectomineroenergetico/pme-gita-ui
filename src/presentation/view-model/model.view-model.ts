import { ModelState } from './../../data/state/models/model.redux.slice';
import { createModelInteractor } from '../../domain/interactors/use-cases/ModelInteractor';
import { BandsInfo, ModelRequest, ModelResponseKMeans, ModelResponseSVM, ChartsForSVM, ModelResponseNN } from '../../domain/entity/structures/model.structure';
import { KeyValueSelect } from '../../domain/entity/structures/common.structure';
import { createReduxModelGateway } from '../../data/state/models/ReduxModelGateway';
import store from '../../store';
import { EdgeDevViewModel } from './edgedevs.view-model';
import { EdgeDevInteractor } from '../../domain/interactors/use-cases/EdgeDevInteractor';

const modelGateway = createReduxModelGateway(store.dispatch);
const modelInteractor = createModelInteractor(modelGateway);

export interface ModelViewModel {
  techniquesModalOpen: boolean;
  errorMessageCharacteristic: string;
  errorMessageTechnique:string;
  characteristicRadioValue:string | undefined;
  techniqueRadioValue:string | undefined;
  doRadioValue:string | undefined;
  bands:Array<BandsInfo>;
  selectClassProblem: Array<KeyValueSelect>;
  classProblemValue:string | undefined;
  chartModelDataResponse: any;
  KMeansCharts: Array<any> | undefined;
  SVMCharts:ChartsForSVM | undefined;
  NNCharts:any | undefined;
  recalc_features:boolean;
  historyOfAnalyzedModels:Array<any>;
  labelsFound: Object;
  labelsRadioValue:string;
  responseUpdateLabel:any | undefined;
  modelResults:any | undefined;
  timeTechniqueHistogramData:Array<Array<any>> | undefined;
  onChangeValue(value:any, key:string):void;
  getLabels():void;
  changeBandValue(index:number, currentValue: boolean):void;
  selectAllBands(value:boolean):void;
  runModelSelected(file: File, edgeDevsViewModel:EdgeDevViewModel, edgeDevInteractor:EdgeDevInteractor):void;
  formatModelResponse():void;
  resetState():void;
  updateHistoryModel(history:Array<any>):void;
  getResultsModelByName(modelName:string, techniqueSelected:string):void;
  getHistoryModel():void;
  transformHistogramData():Array<Array<number | string>>
}

export function createModelViewModel(
  modelState: ModelState
): ModelViewModel {
  return {
    get timeTechniqueHistogramData():Array<Array<any>> | undefined{
      return modelState.timeTechniqueHistogramData
    },
    get techniquesModalOpen(): boolean {
      return modelState.techniquesModalOpen;
    },

    get labelsFound(): Object{
      return modelState.labelsFound
    },

    get modelResults():any | undefined{
      return modelState.modelResults
    },

    get labelsRadioValue():string{
      return modelState.labelsRadioValue;
    },

    get responseUpdateLabel():any | undefined {
      return modelState.responseUpdateLabel;
    },

    get errorMessageCharacteristic(): string {
      return modelState.errorMessageCharacteristic;
    },

    get errorMessageTechnique(): string {
      return modelState.errorMessageTechnique;
    },

    get characteristicRadioValue():string | undefined {
      return modelState.characteristicRadioValue
    },

    get techniqueRadioValue():string | undefined{
      return modelState.techniqueRadioValue
    },

    get bands():Array<BandsInfo>{
      return modelState.bands
    },

    get recalc_features():boolean{
      return modelState.recalc_features;
    },

    get selectClassProblem(): Array<KeyValueSelect>{
      return modelState.selectClassProblem
    },

    get doRadioValue():string | undefined{
      return modelState.doRadioValue
    },

    get classProblemValue():string | undefined{
      return modelState.classProblemValue
    },

    get chartModelDataResponse(): any{
      return modelState.chartModelDataResponse
    },

    get KMeansCharts(): Array<any> | undefined{
      return modelState.KMeansCharts;
    },

    get SVMCharts():ChartsForSVM | undefined{
      return modelState.SVMCharts;
    },

    get NNCharts():any | undefined{
      return modelState.NNCharts
    },

    get historyOfAnalyzedModels():Array<any>{
      return modelState.historyOfAnalyzedModels;
    },

    onChangeValue(value:any, key:string):void{
      let selectProblemClass=[...this.selectClassProblem];
      modelInteractor.onChangeInputValue(value, key, selectProblemClass);
    },
    
    getLabels():void{
      modelInteractor.getLabels();
    },

    changeBandValue(index:number, currentValue: boolean):void{
      modelInteractor.changeBandValue(index, currentValue);
    },

    selectAllBands(value:boolean):void{
      modelInteractor.selectAllBands(value)
    },

    runModelSelected(file: File, edgeDevsViewModel:EdgeDevViewModel, edgeDevInteractor:EdgeDevInteractor):void{
      const bodyRequest:ModelRequest={
        selected_feature: this.characteristicRadioValue,
        selected_technique: this.techniqueRadioValue,
        selected_bands: this.bands.map((elm => {
          return elm.value
        })),
        do: this.doRadioValue,
        class_problem: this.classProblemValue,
        recalc_features: this.recalc_features
     }
     
      modelInteractor.runModelSelected(bodyRequest, file, edgeDevsViewModel, edgeDevInteractor)
    },

    formatModelResponse():void{
      if(this.techniqueRadioValue==='LINEAR-SVM'){
        let response:ModelResponseSVM={...this.chartModelDataResponse};
        modelInteractor.formatModelResponseSVM(response)
      }else if(this.techniqueRadioValue==='NEURAL-NETWORK' || this.techniqueRadioValue==='RANDOM-FOREST'){
        let response:ModelResponseNN={...this.chartModelDataResponse};
        modelInteractor.formatModelResponseNN(response)
      }else{
        let response:ModelResponseKMeans={...this.chartModelDataResponse};
        modelInteractor.formatModelResponseKMeans(response)
      } 
    },

    resetState():void{
      modelInteractor.resetFormState()
    },

    updateHistoryModel(history:Array<any>):void{
      modelInteractor.updateHistoryModel(history)
    },

    getResultsModelByName(modelName:string, techniqueSelected:string):void{
      modelInteractor.getResultsModelByName(modelName, techniqueSelected)
    },

    getHistoryModel():void{
      modelInteractor.getHistoryModel();
    },

    transformHistogramData():Array<Array<number | string>>{
      return modelInteractor.transformHistogramData(this.modelResults)
    }
  };
}
