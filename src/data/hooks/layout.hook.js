import React, { createContext, useContext, useState } from "react";
import { menuItems } from "../../presentation/constants/menu.constants";
import jwt_decode from "jwt-decode";

import store from "../../store";
import { useAuth } from "./use-auth.hook";
import { createReduxFileUploadGateway } from "../state/fileUpload/ReduxFileuploadGateway";
import { createFileUploadInteractor } from "../../domain/interactors/use-cases/FileUploadInteractor";

export const LayoutContext = createContext();

export const ProvideLayout = ({ children }) => {
  const layout = useProvideLayout();
  return (
    <LayoutContext.Provider value={layout}>{children}</LayoutContext.Provider>
  );
};

export const useLayout = () => {
  return useContext(LayoutContext);
};

export default function useProvideLayout() {
  const menu_items = menuItems;
  const auth = useAuth();

  const fileUploadGateway = createReduxFileUploadGateway(store.dispatch);
  const fileUploadInteractor = createFileUploadInteractor(fileUploadGateway);

  const decodeUsername = () => {
    var decoded = jwt_decode(auth.token);
    return decoded["e-mail"];
  };

  const [username, setUsername] = useState(decodeUsername());

  const updateUsername = (username) => {
    setUsername(username);
  };

  return {
    username,
    menu_items,
    fileUploadInteractor,
    decodeUsername,
    updateUsername,
  };
}
