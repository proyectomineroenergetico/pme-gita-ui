import React, { createContext, useContext, useState } from "react";

export const AuthContext = createContext();

export const ProvideAuth = ({ children }) => {
  const auth = useProvideAuth();
  return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>;
};

export const useAuth = () => {
  return useContext(AuthContext);
};

export default function useProvideAuth() {
  const [token, settoken] = useState(localStorage.getItem("userToken"));

  const signin = (token) => {
    localStorage.setItem("userToken", token);
    settoken(token);
  };

  const signout = () => {
    localStorage.removeItem("userToken");
    settoken(null);
  };

  const decodeToken = () => {};

  const userIsAuthenticated = () => {
    if (localStorage.getItem("userToken")) {
      return true;
    }
    return false;
  };

  return {
    token,
    signin,
    signout,
    decodeToken,
    userIsAuthenticated,
  };
}
