import {
  uploadFile,
  getEdgeDevices,
  getEdgeDeviceInfo,
  changeToogleModal,
  changeInputValue,
} from "./edgedevs.redux.slice";
import { Dispatch } from "react";
import { EdgeDevGateway } from "../../../domain/interactors/gateways/EdgeDevGateway";
import {
  resetResponseError,
  resetResponseSuccess,
  changeModalUpdateMeterState
} from "../edgedevs/edgedevs.redux.slice";
import { resetFileUploadState } from "../fileUpload/fileupload.redux.slice";
import { KeyValue } from "../../../domain/entity/structures/common.structure";

export function createReduxEdgedevGateway(
  dispatch: Dispatch<{}>
): EdgeDevGateway {
  return {
    uploadEdgeDevsByFile(file: File, uploadEdgeDevsByFile:boolean, actionInUpdateMeters:string) {
      return dispatch(uploadFile({file,uploadEdgeDevsByFile, actionInUpdateMeters}));
    },
    onChangeInputValue(keyValue:KeyValue):void{
      dispatch(changeInputValue(keyValue));
    },    
    getEdgeDevicesList() {
      dispatch(getEdgeDevices());
    },
    getEdgeDeviceInfo(siccode: string) {
      dispatch(getEdgeDeviceInfo(siccode));
    },
    toogleModal(key: string, value: boolean) {
      dispatch(changeToogleModal({ key, value }));
    },
    changeModalUpdateMeterState(value:boolean):void{
      if(value){
        dispatch(changeModalUpdateMeterState(false));
      }else{
        dispatch(changeModalUpdateMeterState(true));
      }      
    },
    resetAfterUpload(){
      dispatch(resetResponseError(null));
      dispatch(resetResponseSuccess(null));
      dispatch(resetFileUploadState());
    }

    

    // extractFeatures(characteristicRadioSelected: SchemaValidationRadioGroup, techniqueRadioSelected: SchemaValidationRadioGroup): void{
    //   if (characteristicRadioSelected.isSelectedAnyOption && techniqueRadioSelected.isSelectedAnyOption) {
    //     const requestObject:EdgeDevRadioSelected={
    //       characteristic:characteristicRadioSelected,
    //       technique:techniqueRadioSelected
    //     }        
    //     dispatch(extractFeatures(requestObject));
    //   } else if(!characteristicRadioSelected.isSelectedAnyOption) {
    //     const payload={
    //       message:"¡Debes seleccionar una característica!",
    //       state: 'errorMessageCharacteristic'
    //     }
    //     dispatch(
    //       setErrorRadioInputGroup(payload)
    //     );
    //   } else if(!techniqueRadioSelected.isSelectedAnyOption) {
    //     const payload={
    //       message:"¡Debes seleccionar una técnica!",
    //       state: 'errorMessageTechnique'
    //     }
    //     dispatch(
    //       setErrorRadioInputGroup(payload)
    //     );
    //   }
    // },

    

    
  };
}
