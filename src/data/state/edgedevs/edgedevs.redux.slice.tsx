import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  EdgeDevState,
  EdgeDevsSuccessResponseSlice,
  EdgeDevInfoSuccessResponseSlice,
  EdgeDevRadioSelected,
  FileUploadRequest,
} from "../../../domain/entity/structures/edgedevs.structures";
import EdgeDevApi from "../../api/edgedev.api";
import { HTTP_STATUS } from "../../../presentation/constants/api.constants";
import { KeyValue } from "../../../domain/entity/structures/common.structure";

const edgeDevApi = new EdgeDevApi();

export const uploadFile = createAsyncThunk(
  "edgedevs/addEdgeDevsByFile",
  async (request: FileUploadRequest) => {
    const response = await edgeDevApi.uploadEdgeDevsByFile(
      request.file,
      request.actionInUpdateMeters
    );
    const respJson = await response.json();
    return {
      responseData: respJson,
      status: response.status,
      isUpdate: request.uploadEdgeDevsByFile,
    };
  }
);

export const getEdgeDevices = createAsyncThunk(
  "edgedevs/getEdges",
  async () => {
    const response = await edgeDevApi.getEdgeDevices();
    const respJson = await response.json();
    return {
      responseData: respJson,
      status: response.status,
    } as EdgeDevsSuccessResponseSlice;
  }
);

export const getEdgeDeviceInfo = createAsyncThunk(
  "edgedevs/getEdgeDeviceInfo",
  async (siccode: string) => {
    const response = await edgeDevApi.getEdgeDeviceInfo(siccode);
    const respJson = await response.json();
    // console.log(thunkApi.getState().edgeDevsState);
    return {
      responseData: respJson,
      status: response.status,
      siccode: siccode,
    } as EdgeDevInfoSuccessResponseSlice;
  }
);

export const extractFeatures = createAsyncThunk(
  "edgedevs/extractFeatures",
  async (requestObject: EdgeDevRadioSelected, thunkApi: any) => {
    const response = await edgeDevApi.extractFeatures(
      thunkApi.getState().edgeDevsState.siccodeTarget,
      requestObject.characteristic.radioBtnValue
    );
    const respJson = await response.json();

    // TODO: Debo resetear el mensaje de error
    return {
      responseData: respJson,
      status: response.status,
    } as EdgeDevsSuccessResponseSlice;
  }
);

export const edgedevsSlice = createSlice({
  name: "edgedevs",
  initialState: {
    edgedevs: [],
    menuItemsForEdge: [
      {
        name: "Dispositivos",
        redirectTo: "",
        isActive: false,
      },
      {
        name: "Características",
        redirectTo: "",
        isActive: false,
      },
    ],
    isLoaderShown: false,
    loaderMessage: null,
    responseError: null,
    responseSuccess: null,
    characteristicsModalOpen: false,
    siccodeTarget: null,
    edgeDevTargetData: null,
    updateEdgesModalIsOpen: false,
    responseSuccessUpdateMeters: null,
    actionInUpdateMeters: "deleteMetersOption",
  } as EdgeDevState,
  reducers: {
    resetResponseError: (state, action) => {
      state.responseError = action.payload;
    },

    resetResponseSuccess: (state, action) => {
      state.responseSuccess = action.payload;
    },

    changeToogleModal: (state, action) => {
      if ((action.payload.key = "characteristicsModalOpen")) {
        state.characteristicsModalOpen = action.payload.value;
      }
    },
    resetEdgeDevPowerData: (state, action) => {
      state.edgeDevTargetData = action.payload;
    },

    changeInputValue: (state: any, action: PayloadAction<KeyValue>) => {
      state[action.payload.key] = action.payload.value;
    },

    changeModalUpdateMeterState: (state, action) => {
      state.updateEdgesModalIsOpen = action.payload;
    },
  },
  extraReducers: (builder) => {
    // upload  File
    builder.addCase(uploadFile.pending, (state, action) => {
      state.loaderMessage = "Cargando archivo, por favor espere ...";
      state.isLoaderShown = true;
    });
    builder.addCase(uploadFile.fulfilled, (state, action) => {
      state.isLoaderShown = false;
      state.loaderMessage = null;
      if (action.payload.status === HTTP_STATUS.OK) {
        if (!action.payload.isUpdate) {
          state.responseSuccess = action.payload.responseData.message;
        } else {
          state.responseSuccessUpdateMeters =
            action.payload.responseData.message;
        }
        state.edgedevs = action.payload.responseData.siccodes;
      } else if (action.payload.status === HTTP_STATUS.BAD_REQUEST) {
        state.responseError = action.payload.responseData.message;
      }
    });
    builder.addCase(uploadFile.rejected, (state, action) => {
      state.isLoaderShown = false;
    });

    // get EdgeDevs
    builder.addCase(getEdgeDevices.pending, (state, action) => {
      state.loaderMessage = "Cargando dispositivos, por favor espere ...";
      state.isLoaderShown = true;
    });
    builder.addCase(getEdgeDevices.fulfilled, (state, action) => {
      state.loaderMessage = null;
      state.isLoaderShown = false;
      state.edgedevs = action.payload.responseData.data.siccodes;
    });
    builder.addCase(getEdgeDevices.rejected, (state) => {
      state.loaderMessage = null;
      state.isLoaderShown = true;
    });

    // get EdgeDev data
    builder.addCase(getEdgeDeviceInfo.pending, (state, action) => {
      state.loaderMessage =
        "Cargando información de dispositivo, por favor espere ...";
      state.isLoaderShown = true;
    });
    builder.addCase(getEdgeDeviceInfo.fulfilled, (state, action) => {
      state.loaderMessage = null;
      state.isLoaderShown = false;
      state.siccodeTarget = action.payload.siccode;
      state.edgeDevTargetData = action.payload.responseData.data;
    });
    builder.addCase(getEdgeDeviceInfo.rejected, (state) => {
      state.loaderMessage = null;
      state.isLoaderShown = true;
    });

    // get data by characteristic and by meter
    builder.addCase(extractFeatures.pending, (state, action) => {
      state.loaderMessage = `Extrayendo características para el medidor ${state.siccodeTarget}, por favor espere ...`;
      state.isLoaderShown = true;
    });
    builder.addCase(extractFeatures.fulfilled, (state, action) => {
      state.loaderMessage = null;
      state.isLoaderShown = false;
      state.siccodeTarget = action.payload.siccode;
    });
  },
});

export const {
  resetResponseError,
  resetResponseSuccess,
  changeToogleModal,
  resetEdgeDevPowerData,
  changeModalUpdateMeterState,
  changeInputValue,
} = edgedevsSlice.actions;
export const selectEdgedevs = (state: EdgeDevState) => state.edgedevs;
export const selectEdgeDevTargetData = (state: EdgeDevState) =>
  state.edgeDevTargetData;

export default edgedevsSlice.reducer;
