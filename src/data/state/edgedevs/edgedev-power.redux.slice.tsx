import {  createSlice, PayloadAction } from "@reduxjs/toolkit";
import { EdgedevPowerState } from "../../../domain/entity/structures/stats.structure";


export const powerEdgedevsSlice = createSlice({
  name: "edgedevs",
  initialState: {
    activePowerSchema:[
      {
        name:"Time",
        type: "date",
        format: "%-m/%-d/%Y"
      }, {
        name: "Potencia activa",
        type: "number"
      } , {
        name: "Potencia reactiva",
        type: "number"
      }
    ],
    timeSeriesDs: {
        type: 'timeseries',
        renderAt: 'container',
        width: '100%',
        height: '700',
        dataSource: {
            caption: { text: 'Series de tiempo de consumo' },
            data: null,
            yAxis: [{
                plot: [{
                    value: 'Potencia (kb)'
                }]
            }]
        }
    },
    powerDataEdgeDev:null
  } as EdgedevPowerState,
  reducers: {
    updateTimeSeriesData:(state, action:PayloadAction<any>)=>{
      state.powerDataEdgeDev=action.payload;
    }
  },
  extraReducers: (builder) => {
   
  },
});

export const {
 updateTimeSeriesData
} = powerEdgedevsSlice.actions;

export const selectActivePowerSchema = (state: EdgedevPowerState) => state.activePowerSchema;


export default powerEdgedevsSlice.reducer;
