import { Dispatch } from "react";
import { EdgeDevPowerGateway } from "../../../domain/interactors/gateways/EdgeDevPowerGateway";
import { updateTimeSeriesData } from "./edgedev-power.redux.slice";

export function createReduxEdgedevPowerGateway(
  dispatch: Dispatch<{}>
): EdgeDevPowerGateway {
  return {
    updateTimeSeriesData(timeseriesConfig:any) {
      dispatch(updateTimeSeriesData(timeseriesConfig));
    },
  };
}
