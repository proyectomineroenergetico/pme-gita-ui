import { Dispatch } from "react";
import { FileUploadGateway } from "../../../domain/interactors/gateways/FileUploadGateway";
import { updateFileValue, resetFileValues } from "./fileupload.redux.slice";

export function createReduxFileUploadGateway(
  dispatch: Dispatch<{}>
): FileUploadGateway {
  return {
    updateFileInputValue(isValidTheUploadedFile: boolean) {
      dispatch(updateFileValue(isValidTheUploadedFile));
    },
    resetFileValues() {
      dispatch(resetFileValues(null));
    },
  };
}
