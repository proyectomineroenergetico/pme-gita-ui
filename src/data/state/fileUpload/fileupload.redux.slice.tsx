import { createSlice } from "@reduxjs/toolkit";
import { FileUploadedState } from "../../../domain/entity/structures/layout.structure";

const initialState:any= {
  isValidTheUploadedFile: null,
};

export const fileUploadSlice = createSlice({
  name: "fileUpload",
  initialState,
  reducers: {
    resetFileUploadState:()=>initialState,
    updateFileValue: (state, action) => {
      state.isValidTheUploadedFile = action.payload;
    },
    resetFileValues: (state, action) => {
      state.isValidTheUploadedFile = action.payload;
    },
  },
});

export const { updateFileValue, resetFileValues, resetFileUploadState } = fileUploadSlice.actions;
export const selectFileUploadState = (state: FileUploadedState) => state;
export default fileUploadSlice.reducer;
