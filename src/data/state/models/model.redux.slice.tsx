import { createSlice, PayloadAction, createAsyncThunk } from "@reduxjs/toolkit";
import ModelApi from "../../api/model.api";
import { HTTP_STATUS } from "../../../presentation/constants/api.constants";
import {
  BandsInfo,
  IndexValue,
  ModelRequest,
  ModelResponseKMeans,
  ModelResponseSVM,
  ChartsForSVM,
  ModelResponseNN,
} from "../../../domain/entity/structures/model.structure";
import {
  KeyValueSelect,
  KeyValue,
} from "../../../domain/entity/structures/common.structure";

const modelApi = new ModelApi();

export interface ModelState {
  techniquesModalOpen: boolean;
  errorMessageCharacteristic: string;
  errorMessageTechnique: string;
  characteristicRadioValue: string | undefined;
  doRadioValue: string | undefined;
  techniqueRadioValue: string | undefined;
  selectedCharacteristic: string;
  recalc_features: boolean;
  bands: Array<BandsInfo>;
  selectClassProblem: Array<KeyValueSelect>;
  classProblemValue: string | undefined;
  chartModelDataResponse:
    | ModelResponseKMeans
    | ModelResponseSVM
    | ModelResponseNN;
  KMeansCharts: Array<any> | undefined;
  SVMCharts: ChartsForSVM | undefined;
  NNCharts: any | undefined;
  historyOfAnalyzedModels: Array<any>;
  labelsFound: Object;
  labelsRadioValue: string;
  responseUpdateLabel: any | undefined;
  modelResults: any | undefined;
  timeTechniqueHistogramData: Array<Array<any>> | undefined;
}

export const getLabels = createAsyncThunk("models/getLabels", async () => {
  const response = await modelApi.getLabels();
  const respJson = await response.json();
  return {
    responseData: respJson,
    status: response.status,
  };
});

export const getResultsModelByName = createAsyncThunk(
  "models/getResultsModelByName",
  async (modelName: string) => {
    const response = await modelApi.getResultsModelByName(modelName);
    if (response.status === HTTP_STATUS.OK) {
      const respJson = await response.json();
      return respJson;
    }
    return null;
  }
);

export const getHistoryModel = createAsyncThunk(
  "models/getHistoryModel",
  async () => {
    const response = await modelApi.getHistoryModel();
    if (response.status === HTTP_STATUS.OK) {
      const respJson = await response.json();
      return respJson;
    }
    return null;
  }
);

export const updateLabels = createAsyncThunk(
  "models/updateLabels",
  async (file: File) => {
    const response = await modelApi.uploadLabelsByFile(file);
    if (response.status === HTTP_STATUS.OK) {
      const respJson = await response.json();
      return {
        status: response.status,
        data: respJson.siccodes,
      };
    } else {
      const respJson = await response.json();
    }
  }
);

export const runModelSelected = createAsyncThunk(
  "models/runModelSelected",
  async (bodyRequest: ModelRequest) => {
    if (!bodyRequest.class_problem) {
      bodyRequest.class_problem = "";
    }
    if (!bodyRequest.do) {
      bodyRequest.do = "";
    }
    if (!bodyRequest.selected_bands) {
      bodyRequest.selected_bands = [];
    }
    if (!bodyRequest.selected_feature) {
      bodyRequest.selected_feature = "";
    }
    if (!bodyRequest.selected_technique) {
      bodyRequest.selected_technique = "";
    }
    const response = await modelApi.runModelSelected(bodyRequest);
    // const respJson: ModelResponseKMeans | ModelResponseSVM = await response.json();
    return {
      responseData: "",
      status: response.status,
    };
  }
);

const initBands: any = [
  {
    frequency: "0-3.472e",
    name: "Banda_1",
    enabled: false,
    value: 0,
  },
  {
    frequency: "3.472e-5 - 6.946e-5",
    exponencial: "-5",
    name: "Banda_2",
    enabled: false,
    value: 0,
  },
  {
    frequency: "6.946e-5 - 1.0416e-4",
    exponencial: "-5",
    name: "Banda_3",
    enabled: false,
    value: 0,
  },
  {
    frequency: "1.0416e-4 - 1.388e-4",
    name: "Banda_4",
    enabled: false,
    value: 0,
  },
  {
    frequency: "1.388e-4 - 1.7361e-4",
    name: "Banda_5",
    enabled: false,
    value: 0,
  },
  {
    frequency: "1.7361e-4 - 2.083e-4",
    name: "Banda_6",
    enabled: false,
    value: 0,
  },
  {
    frequency: "2.083e-4 - 2.4305e-4",
    name: "Banda_7",
    enabled: false,
    value: 0,
  },
  {
    frequency: "2.4305e-4 - 2.77e-4",
    name: "Banda_8",
    enabled: false,
    value: 0,
  },
  {
    frequency: "0 - 1.388e-4",
    name: "Bandas_1-4",
    enabled: false,
    value: 0,
  },
  {
    frequency: "1.388e-4 - 2.77e-4",
    name: "Bandas_5-8",
    enabled: false,
    value: 0,
  },
  {
    frequency: "0 - 2.77e-4",
    name: "Banda_todas",
    enabled: false,
    value: 0,
  },
];

const initClassProblem: any = [
  {
    text: "Planta 1",
    value: "label_planta",
  },
  {
    text: "Planta 2",
    value: "label_planta2",
  },
  {
    text: "Planta 3",
    value: "label_planta3",
  },
  {
    text: "Tensión",
    value: "label_tension",
  },
  {
    text: "Tensión 2",
    value: "label_tension2",
  },
];

export const modelSlice = createSlice({
  name: "models",
  initialState: {
    selectedCharacteristic: undefined,
    errorMessageCharacteristic: undefined,
    errorMessageTechnique: undefined,
    characteristicRadioValue: undefined,
    techniqueRadioValue: undefined,
    doRadioValue: undefined,
    recalc_features: false,
    bands: initBands,
    selectClassProblem: initClassProblem,
    classProblemValue: undefined,
    chartModelDataResponse: undefined,
    KMeansCharts: undefined,
    SVMCharts: undefined,
    NNCharts: undefined,
    historyOfAnalyzedModels: [],
    labelsFound: {},
    labelsRadioValue: "YES-LABELS",
    responseUpdateLabel: undefined,
    modelResults: undefined,
    timeTechniqueHistogramData: undefined,
  } as ModelState,
  reducers: {
    asignTheSelectedCharacteristic: (state, action) => {
      state.selectedCharacteristic = action.payload;
    },
    setHistoryModel: (state, action) => {
      state.historyOfAnalyzedModels = action.payload;
    },
    changeInputValue: (state: any, action: PayloadAction<KeyValue>) => {
      state[action.payload.key] = action.payload.value;
    },
    setErrorRadioInputGroup: (state: any, action: any) => {
      state[action.payload.state] = action.payload.message;
    },
    setIndexBandValue: (state, action: PayloadAction<IndexValue>) => {
      const bands = [...state.bands];
      bands[action.payload.index].enabled = action.payload.enabled;
      bands[action.payload.index].value = action.payload.value;
      state.bands = bands;
    },
    changeAllBands: (state, action: PayloadAction<boolean>) => {
      let bands = [...state.bands];
      for (let index = 0; index < bands.length; index++) {
        const element = bands[index];
        element.enabled = action.payload;
        element.value = action.payload ? 1 : 0;
      }
      state.bands = bands;
    },
    setKMeansCharts: (state, action: PayloadAction<Array<any>>) => {
      state.KMeansCharts = action.payload;
    },
    setSVMCharts: (state, action: PayloadAction<ChartsForSVM>) => {
      state.SVMCharts = action.payload;
    },
    setNNCharts: (state, action: PayloadAction<any>) => {
      state.NNCharts = action.payload;
    },

    resetFormState: (state, action) => {
      state.characteristicRadioValue = action.payload;
      state.techniqueRadioValue = action.payload;
      state.doRadioValue = action.payload;
      state.classProblemValue = action.payload;
      state.selectClassProblem = initClassProblem;
      state.bands = initBands;
    },

    changeSelectClassProblemState: (state, action) => {
      state.selectClassProblem = action.payload;
    },

    resetResponseUpdateLabel: (state, action) => {
      state.responseUpdateLabel = action.payload;
    },

    setHistogramChartForTimeTechnique: (state, action) => {
      state.timeTechniqueHistogramData = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getLabels.fulfilled, (state, action) => {
      if (action.payload.status === HTTP_STATUS.OK) {
        state.labelsFound = action.payload.responseData.siccodes;
        // state.responseError = action.payload.responseData.message;
      }
    });
    builder.addCase(runModelSelected.fulfilled, (state, action) => {
      if (action.payload.status === HTTP_STATUS.OK) {
        // state.chartModelDataResponse=action.payload.responseData;
        // state.responseError = action.payload.responseData.message;
      }
    });
    builder.addCase(updateLabels.fulfilled, (state, action) => {
      state.responseUpdateLabel = action.payload;
    });
    builder.addCase(getResultsModelByName.fulfilled, (state, action) => {
      if (action.payload.status === "success") {
        state.modelResults = action.payload.results;
      }
    });
  },
});

export const {
  setHistogramChartForTimeTechnique,
  asignTheSelectedCharacteristic,
  resetResponseUpdateLabel,
  changeInputValue,
  changeAllBands,
  setHistoryModel,
  changeSelectClassProblemState,
  setErrorRadioInputGroup,
  resetFormState,
  setIndexBandValue,
  setKMeansCharts,
  setSVMCharts,
  setNNCharts,
} = modelSlice.actions;
// export const selectFileUploadState = (state: FileUploadedState) => state;
export default modelSlice.reducer;
