import { Dispatch } from "react";
import { ModelGateway } from "../../../domain/interactors/gateways/ModelGateway";
import { asignTheSelectedCharacteristic, setHistoryModel,getLabels, changeInputValue, resetFormState, setIndexBandValue, changeAllBands, runModelSelected, setSVMCharts, setKMeansCharts, setNNCharts, changeSelectClassProblemState, getResultsModelByName, getHistoryModel, updateLabels, resetResponseUpdateLabel, setHistogramChartForTimeTechnique } from "../models/model.redux.slice";
import { KeyValue, KeyValueSelect } from "../../../domain/entity/structures/common.structure";
import { ModelRequest, ChartsForSVM } from "../../../domain/entity/structures/model.structure";

export function createReduxModelGateway(
  dispatch: Dispatch<{}>
): ModelGateway {
  return {
    asignTheSelectedCharacteristic(value: string) {
      dispatch(asignTheSelectedCharacteristic(value));
    },
    onChangeInputValue(keyValue:KeyValue){
      dispatch(changeInputValue(keyValue));
    },
    getLabels(){
      dispatch(getLabels())
    },
    changeBandValue(index:number, currentValue: boolean){
      dispatch(
        setIndexBandValue({index:index,
        value: currentValue ? 0 : 1,
        enabled: !currentValue
      }))
    },
    selectAllBands(value:boolean){
      dispatch(changeAllBands(value))
    },
    async runModelSelected(bodyRequest:ModelRequest):Promise<any>{
      return dispatch(runModelSelected(bodyRequest))
    },
    setKMeansCharts(chartConfig:Array<any>):void{
      dispatch(setKMeansCharts(chartConfig))
    },
    setSVMCharts(chartConfig:ChartsForSVM):void{
      dispatch(setSVMCharts(chartConfig))
    },

    setNNCharts(chartConfig:any):void{
      dispatch(setNNCharts(chartConfig))
    },
    resetFormState():void{
      dispatch(resetFormState(undefined))
    },
    changeSelectClassProblemState(selectClassProblem: Array<KeyValueSelect>):void{
      dispatch(changeSelectClassProblemState(selectClassProblem))
    },

    setHistoryModel(history:Array<any>):void{
      dispatch(setHistoryModel(history))
    },

    getResultsModelByName(modelName:string):void{
     return dispatch(getResultsModelByName(modelName))
    },

    getHistoryModel():void{
      return dispatch(getHistoryModel())
    },

    async updateLabels(file:File){
      return dispatch(updateLabels(file));
    },

    resetResponseUpdateLabel():void{
      return dispatch(resetResponseUpdateLabel(undefined))
    },

    setHistogramChartForTimeTechnique(chartData:Array<Array<any>> | undefined):void{
      dispatch(setHistogramChartForTimeTechnique(chartData))
    }
  };
}
