import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {
  IUser,
  LoginState,
} from "../../../domain/entity/structures/user.structure";
import AuthApi from "../../api/auth.service";
import { HTTP_STATUS } from "../../../presentation/constants/api.constants";

const authApi = new AuthApi();

export const loginUser = createAsyncThunk("auth/login", async (user: IUser) => {
  const response = await authApi.login(user);
  const respJson = await response.json();
  return { responseData: respJson, status: response.status };
});

export const authSlice = createSlice({
  name: "auth",
  initialState: {
    emailError: null,
    emailValue: "",
    passwordError: null,
    passwordValue: "",
    pendingLogin: false,
    responseError: null,
    successMessage: "",
  } as LoginState,
  reducers: {
    resetLoginSuccessResponse: (state, action) => {
      state.successMessage = action.payload;
      state.emailValue = action.payload || "";
      state.emailError = action.payload;
      state.passwordError = action.payload;
      state.passwordValue = action.payload || "";
    },
    resetResponseError: (state, action) => {
      state.responseError = action.payload;
    },
    updateEmailState: (state, action) => {
      state.emailError = action.payload.error;
      state.emailValue = action.payload.value;
    },
    updatePasswordState: (state, action) => {
      state.passwordError = action.payload.error;
      state.passwordValue = action.payload.value;
    },
    logoutUser:(state,action)=>{

    }
  },
  extraReducers: (builder) => {
    builder.addCase(loginUser.pending, (state) => {
      state.pendingLogin = true;
    });
    builder.addCase(loginUser.fulfilled, (state, action) => {
      state.pendingLogin = false;
      if (action.payload.status === HTTP_STATUS.NOT_FOUND) {
        state.responseError = action.payload.responseData.message;
      } else if (action.payload.status === HTTP_STATUS.OK) {
        state.successMessage = {
          message: action.payload.responseData.message,
          status: action.payload.responseData.status,
          auth_token: action.payload.responseData.auth_token,
        };
      }
    });
    builder.addCase(loginUser.rejected, (state, action) => {
      state.pendingLogin = false;
    });
  },
});

export const {
  updatePasswordState,
  updateEmailState,
  resetLoginSuccessResponse,
  resetResponseError,
  logoutUser
} = authSlice.actions;
// export const selectEdgedevs = (state: EdgeDevState) => state.edgedevs;

export default authSlice.reducer;
