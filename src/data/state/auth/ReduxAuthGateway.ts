import { Dispatch } from "react";
import { IUser } from "../../../domain/entity/structures/user.structure";
import { AuthGateway } from "../../../domain/interactors/gateways/AuthGateway";
import {
  loginUser,
  updatePasswordState,
  resetResponseError,
  resetLoginSuccessResponse,
  updateEmailState,
} from "./auth.redux.slice";

export function createReduxAuthGateway(dispatch: Dispatch<{}>): AuthGateway {
  return {
    loginUser(user: IUser) {
      dispatch(loginUser(user));
    },
    updatePasswordState(value: string, error: string) {
      dispatch(updatePasswordState({ value, error }));
    },
    updateEmailState(value: string, error: string) {
      dispatch(updateEmailState({ value, error }));
    },
    resetResponseError() {
      dispatch(resetResponseError(null));
    },
    resetLoginSuccessResponse() {
      dispatch(resetLoginSuccessResponse(null));
    },
  };
}
