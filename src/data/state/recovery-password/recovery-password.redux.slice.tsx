import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {
  RecoveryPasswordState,
  IUser,
  ResetPasswordRequest,
} from "../../../domain/entity/structures/user.structure";
import UserApi from "../../api/user.api";
import { HTTP_STATUS } from "../../../presentation/constants/api.constants";

const userApi = new UserApi();

export const recoveryUser = createAsyncThunk(
  "recoveryPassword/recovery",
  async (email: string) => {
    const response = await userApi.sendEmailRecovery(email);
    if (response.status === HTTP_STATUS.OK) {
      const respJson = await response.json();
      return {
        status: response.status,
        success: true,
        successMessage: respJson.message,
      };
    }
  }
);

export const resetPassword = createAsyncThunk(
  "recoveryPassword/resetPassword",
  async (request: ResetPasswordRequest) => {
    const response = await userApi.updatePassword(request);
    if (response.status === HTTP_STATUS.OK) {
      const respJson = await response.json();
      return {
        status: response.status,
        success: true,
        successMessage: respJson.message,
      };
    } else if (response.status === HTTP_STATUS.BAD_REQUEST) {
      return {
        status: response.status,
        message:
          "El tiempo de recuperación (10 minutos) ha expirado. Debes generar un nuevo enlace de recuperación",
      };
    }
  }
);

const initialState: RecoveryPasswordState = {
  emailError: "",
  passwordError: "",
  confirmPasswordError: "",
  responseError: "",
  isSuccessEmailSend: false,
  isSuccessUpdatePassword: false,
  successMessage: "",
  pendingRecovery: false,
};

export const recoveryPasswordSlice = createSlice({
  name: "recoveryPassword",
  initialState,
  reducers: {
    resetState: () => initialState,
    setErrorEmail: (state, action) => {
      state.emailError = action.payload;
    },
    setErrorPasswordRecovery: (state, action) => {
      state.passwordError = action.payload;
    },
    setErrorConfirmPasswordRecovery: (state, action) => {
      state.confirmPasswordError = action.payload;
    },
    setresponseError: (state, action) => {
      state.responseError = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(recoveryUser.pending, (state) => {
      state.pendingRecovery = true;
    });
    builder.addCase(recoveryUser.fulfilled, (state, action) => {
      state.pendingRecovery = false;
      if (action.payload.status === HTTP_STATUS.OK) {
        state.isSuccessEmailSend = action.payload.success;
        state.successMessage = action.payload.successMessage;
      }
    });
    builder.addCase(resetPassword.fulfilled, (state, action) => {
      if (action.payload.status === HTTP_STATUS.BAD_REQUEST) {
        state.responseError = action.payload.message;
      } else if (action.payload.status === HTTP_STATUS.OK) {
        state.successMessage = action.payload.successMessage;
        state.isSuccessUpdatePassword = action.payload.success;
      }
    });
  },
});

export const {
  resetState,
  setErrorEmail,
  setErrorPasswordRecovery,
  setErrorConfirmPasswordRecovery,
  setresponseError,
} = recoveryPasswordSlice.actions;
// export const selectEdgedevs = (state: EdgeDevState) => state.edgedevs;

export default recoveryPasswordSlice.reducer;
