import { Dispatch } from "react";
import { RecoveryPaswordGateway } from "../../../domain/interactors/gateways/RecoveryPaswordGateway";
import { setErrorEmail, setErrorPasswordRecovery, setErrorConfirmPasswordRecovery, setresponseError, resetPassword } from "./recovery-password.redux.slice";
import { ResetPasswordRequest } from "../../../domain/entity/structures/user.structure";

export function createReduxRecoveryPasswordGateway(dispatch: Dispatch<{}>): RecoveryPaswordGateway {
  return {
    setErrorEmailRecovery(errorMessage:string):void{
      dispatch(setErrorEmail(errorMessage))
    },
    setErrorPasswordRecovery(message:string):void{
      dispatch(setErrorPasswordRecovery(message))
    },
    setErrorConfirmPasswordRecovery(message:string):void{
      dispatch(setErrorConfirmPasswordRecovery(message))
    },
    setresponseError(error:string):void{
      dispatch(setresponseError(error))
    },
    resetPassword(request:ResetPasswordRequest):void{
      dispatch(resetPassword(request))

    }
  };
}
