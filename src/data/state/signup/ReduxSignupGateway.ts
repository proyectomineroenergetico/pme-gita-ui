import { Dispatch } from "react";

import { SignupGateway } from "../../../domain/interactors/gateways/SignupGateway";
import { updatePasswordState, updateEmailState, signupUser } from "../signup/signup.redux.slice";
import { IUser } from "../../../domain/entity/structures/user.structure";

export function createReduxSignupGateway(dispatch: Dispatch<{}>): SignupGateway {
  return {
    updatePasswordState(value: string, error: string) {
      dispatch(updatePasswordState({ value, error }));
    },
    updateEmailState(value: string, error: string) {
      dispatch(updateEmailState({ value, error }));
    },
    signup(user:IUser):void{
      dispatch(signupUser(user));
    }

  };
}
