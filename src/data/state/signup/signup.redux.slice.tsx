import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  SignupState, IUser, ResponseSignup
} from "../../../domain/entity/structures/user.structure";
import { HTTP_STATUS } from "../../../presentation/constants/api.constants";
import UserApi from "../../api/user.api";

const userApi = new UserApi();

export const signupUser = createAsyncThunk("signup/create", async (user: IUser) => {
  const response = await userApi.signup(user);
  
  let respJson:ResponseSignup;
  if(response.status===HTTP_STATUS.CREATED){
    respJson = await response.json();
    return {
      message: respJson.message,
      auth_token: respJson.auth_token,
      status: response.status
    };
  }else{
    respJson = await response.json();
    return {
      message: respJson?.message || 'Ha ocurrido un error interno, intenta mas tarde.',
      status: response.status
    }
  }
});

const initialState: SignupState= {
  emailError:'',
  emailValue:'',
  passwordError:'',
  passwordValue:'',
  pendingSignup:false,
  responseSuccess: undefined,
  responseError: undefined,
};

export const signupSlice = createSlice({
  name: "signup",
  initialState,
  reducers: {
    resetSignupState:()=>initialState,
    updateEmailState: (state, action) => {
      state.emailError = action.payload.error;
      state.emailValue = action.payload.value;
    },
    updatePasswordState: (state, action) => {
      state.passwordError = action.payload.error;
      state.passwordValue = action.payload.value;
    },
    resetSignupResponseError:(state, action)=>{
      state.responseError=action.payload;
    }
  },
  extraReducers: (builder) => {
    builder.addCase(signupUser.pending, (state) => {
      state.pendingSignup = true;
    });
    builder.addCase(signupUser.fulfilled, (state, action:PayloadAction<ResponseSignup>) => {
      state.pendingSignup = false;
      if(action.payload.status===HTTP_STATUS.CREATED){
        state.responseSuccess=action.payload;
      }else{
        state.responseError=action.payload;
      }
    });
   
  },
});

export const {
  updateEmailState,
  updatePasswordState,
  resetSignupState,
  resetSignupResponseError
} = signupSlice.actions;
// export const selectEdgedevs = (state: EdgeDevState) => state.edgedevs;

export default signupSlice.reducer;
