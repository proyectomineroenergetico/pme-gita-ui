import IEdgeDevsRepository from "../../domain/respository/edgedevs.repository";
import { API_ROUTES } from "../../presentation/constants/api.constants";
import HttpBaseService from "../http-base";
import { fi } from "date-fns/esm/locale";

export default class EdgeDevApi
  extends HttpBaseService
  implements IEdgeDevsRepository {
  uploadEdgeDevsByFile(file: any, actionInUpdateMeters?:string): Promise<Response> {
    const formData = new FormData();
    formData.append("file", file);
    if(actionInUpdateMeters){
      if(actionInUpdateMeters==='deleteMetersOption'){
        return this.post(`${API_ROUTES.UPLOAD_FILE}&delete_meters=True`, formData, null, true, true);
      }else{
        return this.post(`${API_ROUTES.UPLOAD_FILE}&delete_meters=False`, formData, null, true, true);
      }
    }else {
      return this.post(API_ROUTES.UPLOAD_FILE, formData, null, true, true);
    }
    
  }

  getEdgeDevices(): Promise<Response> {
    return this.get(API_ROUTES.ALL_METERS, null, true);
  }

  getEdgeDeviceInfo(siccode: string): Promise<Response> {
    return this.get(`${API_ROUTES.EDGEDEV_DATA}${siccode}`, null, true);
  }

  extractFeatures(siccode: string, characteristic: string): Promise<Response> {
    return this.get(`${API_ROUTES.EDGEDEV_DATA}${siccode}`, null, true);
  }
}
