import { IUser } from "../../domain/entity/structures/user.structure";
import AuthRepository from "../../domain/respository/auth-repository";
import { API_ROUTES } from "../../presentation/constants/api.constants";
import HttpBaseService from "../http-base";

export default class AuthApi extends HttpBaseService implements AuthRepository {
  showErrorResponseNotification(error: any): void {
    throw new Error("Method not implemented.");
  }
  login(user: IUser): Promise<Response> {
    return this.post(API_ROUTES.LOGIN, user, null);
  }
}
