import HttpBaseService from "../http-base";
import IModelRepository from "../../domain/respository/model.repository";
import { API_ROUTES } from "../../presentation/constants/api.constants";
import { ModelRequest } from "../../domain/entity/structures/model.structure";

export default class ModelApi
  extends HttpBaseService
  implements IModelRepository {
  getHistoryModel(): Promise<Response> {
    return this.get(`${API_ROUTES.HISTORY_MODEL}?all=True`, null, true)
  }
  getResultsModelByName(model_name: string): Promise<Response> {
    return this.get(`${API_ROUTES.PERFORMANCE}?model_name=${model_name}`,null, true)
  }
  runModelSelected(bodyRequest: ModelRequest): Promise<Response> {
    return this.post(`${API_ROUTES.FEAT_TECH}`, bodyRequest, null, true);
  }
  getLabels(): Promise<Response> {
    return this.get(`${API_ROUTES.LABELS}`, null, true)
  }
  uploadLabelsByFile(file: any): Promise<Response> {
    const formData = new FormData();
    formData.append("file", file);
    return this.post(API_ROUTES.LABELS, formData, null, true, true);
  }
}
