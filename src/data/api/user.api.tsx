import { API_ROUTES } from "../../presentation/constants/api.constants";
import HttpBaseService from "../http-base";
import IUserRepository from "../../domain/respository/IUserRepository";
import { ResetPasswordRequest, IUser } from "../../domain/entity/structures/user.structure";

export default class UserApi
  extends HttpBaseService
  implements IUserRepository {
  updatePassword(request: ResetPasswordRequest): Promise<Response> {
    return this.post(`${API_ROUTES.USER.RESET}`, request,null, false);
  }
  sendEmailRecovery(email: string): Promise<Response> {
    return this.post(`${API_ROUTES.USER.RECOVERY}`, {email},null, false);
  }
  signup(user:IUser):Promise<Response>{
    return this.post(`${API_ROUTES.USER.SIGNUP}`, user,null,false);
  }
}
