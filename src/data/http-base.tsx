import { ENVIRONMENT } from "../presentation/constants/api.constants";

export default class HttpBaseService {
  private baseUrl = `${ENVIRONMENT.IP_BASE}api/`;
  protected get(relativeUrl: string, headers: any, auth: boolean = false) {
    if (auth) {
      headers = {
        ...headers,
        Authorization: localStorage.getItem("userToken"),
      };
    }
    return fetch(`${this.baseUrl}${relativeUrl}`, {
      method: "GET",
      headers: headers,
    });
  }

  protected post(
    relativeUrl: string,
    body: any,
    headers: any,
    auth: boolean = false,
    uploadFile = false
  ) {
    if (auth) {
      headers = {
        ...headers,
        Authorization: localStorage.getItem("userToken"),
      };
    }
    if (!uploadFile) {
      headers = {
        ...headers,
        "Content-Type": "application/json",
      };
    }
    return fetch(`${this.baseUrl}${relativeUrl}`, {
      method: "POST",
      headers: {
        ...headers,
      },
      body: uploadFile ? body : JSON.stringify(body),
    });
  }
}
